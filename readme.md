# Matcha

Seconds **projet scolaire** de la branche Web de l'école [42 Lyon](https://www.42lyon.fr/), il doit être fait en équipe de deux étudiants.

## Objectif

Ce second projet nous introduit à un outil plus évolué pour réaliser vos applications web : le micro-framework.  
Nous sommes invités à réaliser, dans le langage de notre choix, un site de rencontres.  
Les interactions entre utilisateurs seront au coeur du projet.  
Les détails du projet sont décrits dans le [sujet](https://gitlab.com/charles.garoux/matcha/-/blob/master/docs/matcha.fr.pdf).

## Résultat

Le projet a été développé avec le micro-framework  [**Lumen**](https://lumen.laravel.com/)  de Laravel avec les contraintes du sujet.  
Une bibliothèque en **PHP** ont été développées pour le projet avec en grandes parties du code réutilisable pour d’autre projet en **Lumen** ou **PHP**.  
  
Le projet a été validé par 3 correcteurs durant la soutenance ainsi que des bonus :

-   Gestion de tous les genres
-   Carte interactive
-   Les photos de profils peuvent être des GIFs
-   Ajout d'image de profil en "drag and drop"

# Readme du rendu de projet

## Prerequis

1. Le fichier de debug (sont chemin etant specifier dans .env) doit avoir les droits d'ecriture pour tous le monde.

2. Creer le .env avec les données requises (voir section "Variable d'environnement de l'application").

3. Initialiser la BDD (voir section "Docker")

4. Hydrater la BDD (voir section "Docker")

## Variable d'environnement de l'application

Le fichier .env contient toutes les informations importantes pour le fonctionnement de l'application.

Un fichier d'exemple est fournit pour aider a faire le .env :
- .env.exemple (contient un exemple fictif du fichier .env)

Le fichier .env doit etre cree au moment de l'installation de l'application.

Voici les nouvelles variables (non present dans Lumen) et ce a quoi elles correspondent :

```
...
LOG_FILE_PATH=chemin absolue vers le fichier de journalisation
REQUEST_ID_LENGTH=la longueur de identifiant de requete (pour la journalisation)
DEBUG=mode de l'envoie
AJAX_FREQUENCY=seconds en chaque requet AJAX et element "temps reel" de l'application
KEEPALIVE_VALIDITY=seconds de validite du ticket signalant que l'utilisateur est connecte
CHECK_PASSWORD_STRENGTH=activation de la verification de force de mot de passe
...
```

## Docker

Le projet a ete develope avec docker, voici le depot utilise :
[https://gitlab.com/charles.garoux/docker-matcha](https://gitlab.com/charles.garoux/docker-matcha).

Pour lancer les conteneurs :
```
./server.sh start
```

Pour arreter les conteneurs :
```
./server.sh stop
```

Pour initialiser/reinitialiser la base de donnees :
```
./server.sh migrate
```

Pour hydrater la base de donnees :
```
./server.sh hydrate
```

## Documentations

Le sujet du projet est founie dans le repertoire docs :
- matcha.fr.pdf

## Informations supplementaires pour la correction

### Navigateur Chrome

En cas ou l'application n'accede pas a la geolocalisation par navigateur. Pour corriger cela, sur Chrome il faut mettre en URL "[chrome://flags/#unsafely-treat-insecure-origin-as-secure](chrome://flags/#unsafely-treat-insecure-origin-as-secure)", y ajouter l'adresse de l'application puis relancer Chrome.
> Chrome bloque les sites souhaitant utiliser l'API du navigateur quand il ne sont pas en HTTPS

### Docker-machine
Dans le cas ou l'application ne peu pas lire ou ecrire sur fichier a cause de probleme de droits.
Relancer le script de lancement des conteneurs.

Cela peu arriver lors du premier acces a l'application apres installation.
> Un conflict entre MacOS et Docker-machine avec les droits du systeme de fichier peu poser probleme.
