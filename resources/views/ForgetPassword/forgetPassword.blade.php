@extends('includes.template')

@section('contenue')
    <div class="mt-3 text-center">
        <p class="h1">Forget Password</p>
    </div>

    <form method="POST" class="mt-8 text-center">
        <div class="mx-auto col-lg-3 col-md-4 col-12">
            <div class="mt-3">
                <input type="text" class="form-control" id="login" name="login" placeholder="Login" required>
            </div>

            <div class="mt-3">
                <button class="btn btn-primary mt-3" type="submit">Submit form</button>
            </div>
        </div>
    </form>
@endsection
