@extends('includes.template')

@section('contenue')
    <div class="mt-3 text-center">
        <p class="h1">Modify Password</p>
    </div>
    <form method="POST" id="password" class="mt-8 text-center">
        <div class="mx-auto col-lg-3 col-md-4 col-12">
            <div class="mt-3">
                <span id="password_strength">Strength...</span>
                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password" required>
            </div>

            <div class="mt-3">
                <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required>
            </div>

            <div class="mt-3">
                <button id="submit" class="btn btn-primary mt-3" type="submit">Submit form</button>
            </div>
        </div>
    </form>
@endsection

@section('script')
    <script>
        if ('{{ env('CHECK_PASSWORD_STRENGTH') }}' === '1') {
            let passwordForm = document.getElementById("password");
            let password_strength = document.getElementById("password_strength");
            document.getElementById("submit").disabled = true;

            passwordForm.oninput = CheckPasswordStrength;

            function CheckPasswordStrength(e) {
                var password = e.target.value;

                //if textBox is empty
                if(password.length == 0) {
                    password_strength.innerHTML = "";
                    return;
                }

                //Regular Expressions
                var regex = new Array();
                regex.push("[A-Z]"); //For Uppercase Alphabet
                regex.push("[a-z]"); //For Lowercase Alphabet
                regex.push("[0-9]"); //For Numeric Digits
                regex.push("[$@$!%*#?&]"); //For Special Characters

                var score = 0;

                //Validation for each Regular Expression
                for (var i = 0; i < regex.length; i++) {
                    if((new RegExp (regex[i])).test(password)) {
                        score++;
                    }
                }

                //Validation for Length of Password
                if(score > 2 && password.length > 8) {
                    score++;
                }

                //Display of Status
                var color = "";
                var passwordStrengthText = "";
                var canSubmit = false;

                switch(score) {
                    case 0:
                        canSubmit = false;
                        break;
                    case 1:
                        passwordStrengthText = "Password is Weak.";
                        color = "Red";
                        canSubmit = false;
                        break;
                    case 2:
                        passwordStrengthText = "Password is Good.";
                        color = "darkorange";
                        canSubmit = false;
                        break;
                    case 3:
                        passwordStrengthText = "Password is Good.";
                        color = "Green";
                        canSubmit = true;
                        break;
                    case 4:
                        passwordStrengthText = "Password is Strong.";
                        color = "Green";
                        canSubmit = true;
                        break;
                    case 5:
                        passwordStrengthText = "Password is Very Strong.";
                        color = "darkgreen";
                        canSubmit = true;
                        break;
                }
                document.getElementById("submit").disabled = !canSubmit;
                password_strength.innerHTML = passwordStrengthText;
                password_strength.style.color = color;
            }
        }
        else {
            let password_strength = document.getElementById("password_strength");
            password_strength.innerHTML = "";
        }
    </script>
@endsection
