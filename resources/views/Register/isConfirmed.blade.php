@extends('includes.template')

@section('contenue')
    <div class="mt-7 text-center">
        <p class="h1">Your account is confirmed.</p>
        <div class="mt-3">
            <a href="{{ route('login.form') }}" class="btn btn-primary mt-3" type="submit">Login</a>
        </div>
    </div>
@endsection
