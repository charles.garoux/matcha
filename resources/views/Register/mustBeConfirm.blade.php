@extends('includes.template')

@section('contenue')
    <div class="mt-7 text-center">
        <p class="h1">Please check your email to confirm your account.</p>
    </div>
@endsection
