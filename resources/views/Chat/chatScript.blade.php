<script>

    /* ====================== INITIALISATION ====================== */

    let idContact = {{ $idContact }};
    let idUser = {{ $idUser }};
    let messageListElement = document.getElementById("messageList");

    getLastMessage();
    setInterval("getLastMessage();", {{ env("AJAX_FREQUENCY") * 1000 }});

    let messageButtonElement = document.getElementById("messageButton");
    let messageTextElement = document.getElementById("messageText");

    messageButtonElement.addEventListener('click', sendMessage);

    /* ====================== FUNCTIONS ====================== */

    function messageElementComponent(message)
    {
        if (message['Sender']['id'] === idContact) {
            var element = "<div class=\"card mt-3\">\n" +
                "  <div class=\"card-body\">\n" +
                "    <h5 class=\"card-title\">"+ message['Sender']['login'] +"</h5>\n" +
                "    <p class=\"card-text\">"+ message['text'] +"</p>\n" +
                "  </div>\n" +
                "</div>";
        }
        else {
            var element = "<div class=\"card mt-3 text-right\">\n" +
                "  <div class=\"card-body\">\n" +
                "    <h5 class=\"card-title\">"+ message['Sender']['login'] +"</h5>\n" +
                "    <p class=\"card-text\">"+ message['text'] +"</p>\n" +
                "  </div>\n" +
                "</div>";
        }

        return (element);
    }

    function getLastMessage()
    {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '{{ route('ajax.get.chat', compact('idContact')) }}');
        xhr.onload = function() {
            let messageElementsList;
            let response;
            response = JSON.parse(xhr.responseText);
            if (xhr.status === 200 && response['status'] === 'OK') {
                messageElementsList = "";
                response['messages'].forEach(function (value, key, list) {
                    messageElementsList = messageElementsList + messageElementComponent(list[key]);
                });
            }
            else if (response['reason'] === 'Empty conversation') {
                messageElementsList = response['message'];
            }
            else {
                messageElementsList = "An error in the chat occurred";
            }
            messageListElement.innerHTML = messageElementsList;
        };
        xhr.send();
    }

    function sendMessage() {
        var text = messageTextElement.value;
        if (text.length === 0 || messageButtonElement.disabled === true)
            return;

        messageButtonElement.disabled = true;

        xhr = new XMLHttpRequest();
        xhr.open('POST', '{{ route('ajax.send.message', ['idTarget' => $idContact]) }}');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            response = JSON.parse(xhr.responseText);
            if (xhr.status === 200) {
                if (response['status'] === 'OK' || response['reason'] === '[FAIL] Push notification')
                    messageTextElement.value = "";
                if (response['reason'] !== '') {
                    alert(response['message']);
                }
            }
            else {
                alert('Error : can\'t send message');
            }
            messageButtonElement.disabled = false;
        };
        xhr.send(encodeURI('idContact=' + idContact + '&text='+ text));
    }
</script>
