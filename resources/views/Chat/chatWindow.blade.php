@extends('includes.template')

@section('contenue')
    <div class="row">
        <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-12">
            <ul id="messageList" class="list-unstyled">

            </ul>
            <div class="form-group">
                <textarea class="form-control" id="messageText" rows="3" placeholder="Write a message ..."></textarea>
            </div>
            <button id="messageButton" type="button" class="btn btn-block btn-success">Submit</button>
        </div>
    </div>
@endsection

@section('script')
    @include('Chat.chatScript')
@endsection
