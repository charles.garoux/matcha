@extends('includes.template')

@section('contenue')
<div class="row">
    <div class="col-xlg-5 col-lg-5 col-md-6 col-sm-12 col-12 my-3">
        <div class="card bg-special-blue text-light">
            <div id="{{$Profile->login . 'carouselId'}}" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @if($Profile->photos['profile'])
                        <li data-target="#{{$Profile->login . 'carouselId'}}" data-slide-to="0" class="active"></li>
                    @endif
                    @foreach($Profile->photos['other'] as $key => $Photo)
                        <li data-target="#{{$Profile->login . 'carouselId'}}" data-slide-to="{{ $key+1 }}"></li>
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @if($Profile->photos['profile'])
                        <div class="carousel-item active">
                            <img src="{{ $Profile->photos['profile']->image }}" class="d-block w-100">
                        </div>
                    @endif
                    @foreach($Profile->photos['other'] as $Photo)
                        <div class="carousel-item">
                            <img src="{{ $Photo->image }}" class="d-block w-100">
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#{{$Profile->login . 'carouselId'}}" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#{{$Profile->login . 'carouselId'}}" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="card-body">
                <h5 class="card-title">{{ $Profile->login }}
                    <span id="connected" class="badge badge-success">connected</span>
                    <span id="disconnected" class="badge badge-danger">disconnected</span>
                </h5>
                @if($liked)
                    <a href="{{ route('profile.public.like.btn', [ 'idProfile' => $Profile->id]) }}" class="btn btn-block btn-love">Like</a>
                @else
                    <a href="{{ route('profile.public.like.btn', [ 'idProfile' => $Profile->id]) }}" class="btn btn-block btn-outline-love">Like</a>
                @endif
                <div class="btn-group btn-block btn-group-toggle">
                    @if($reported == false)
                        <button type="button" class="btn btn-sm btn-outline-warning" data-toggle="modal" data-target="#reportModal">Report as false account</button>
                        {{-- Report modal --}}
                        <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-special-blue">
                                    <div class="modal-body">
                                        Are you sure you want to report this user as a false account?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <a href="{{ route('profile.public.report.btn', ['idProfile' =>$Profile->id]) }}" class="btn btn-sm btn-warning">Report as false account</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <a href="{{ route('profile.public.report.btn', ['idProfile' =>$Profile->id]) }}" class="btn btn-sm btn-warning">Unreport as false account</a>
                    @endif
                    @if($blocked == false)
                        <button type="button" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#blockModal">Block this user</button>
                        {{-- Block modal --}}
                        <div class="modal fade" id="blockModal" tabindex="-1" role="dialog" aria-labelledby="blockModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-special-blue">
                                    <div class="modal-body">
                                        Are you sure you want to block this user?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <a href="{{ route('profile.public.block.btn', ['idProfile' =>$Profile->id]) }}" class="btn btn-sm btn-danger">Block this user</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <a href="{{ route('profile.public.block.btn', ['idProfile' =>$Profile->id]) }}" class="btn btn-sm btn-danger">Unblock this user</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xlg-7 col-lg-7 col-md-6 col-sm-12 col-12 my-3">
        <div class="card bg-special-blue text-light">
            <div class="card-header h4">
                Presentation
            </div>
            <div class="card-body">
                <p class="mb-0 font-weight-bold">{{ $Profile->first_name .' '. $Profile->last_name }},</p>
                <p class="card-text">{{ $Profile->biography }}</p>
            </div>
            <div class="card-header h4">
                Stats
            </div>
            <div class="card-body">
                <li class="list-group-item bg-love">
                    Compatibility score : {{ ($Profile->compatibility < 0 ? 0 : $Profile->compatibility) }} point
                </li>
                <li class="list-group-item bg-love">
                   Gender : {{ $Profile->Gender->name }}
                </li>
                <li class="list-group-item bg-love">
                    Attracted by :
                    @foreach($Profile->attractions as $Gender)
                        <span class="badge badge-info">{{ $Gender->name }}</span>
                    @endforeach
                </li>
                <li class="list-group-item bg-warning">
                    Number of points in common : {{ $Profile->commonPoints}}
                </li>
                <li class="list-group-item bg-success">
                    Popularity Score : {{ $Profile->score }} points
                </li>
                <li class="list-group-item text-body bg-light">
                    {{ $Profile->age }} years old
                </li>
            </div>
            <div class="card-header h4">
                Position
            </div>
            <div class="card-body">
                <li class="list-group-item bg-info">
                    {{ round($Profile->distance / 1000, 1) ."km" }} from you
                </li>
                {{-- Map --}}
                <div id="myMap" class="container" style="height: 40em">The map is loading</div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
@include('Map.mapScript')
@include('Profile.connectionStatusScript', ['idUser' => $Profile->id])
<script>
    initMap({{ $Profile->geoCoordinates['lat'] }}, {{ $Profile->geoCoordinates['lon'] }});
</script>
@endsection
