@extends('includes.template')

@section('contenue')
    <div class="row">
        <div class="col-md-6 col-12">
            <div class="list-group mt-3">
                <span class="list-group-item list-group-item-action bg-special-blue text-light mb-3">
                    <div class="d-flex w-100 justify-content-center">
                        <h1 class="mb-0">==== Visits ====</h1>
                    </div>
                </span>
                @foreach($VisitExtendList as $Visit)
                    <a href="{{ route('profile.public', ['idProfile' => $Visit->Sender->id]) }}" class="list-group-item list-group-item-action bg-special-blue text-light">
                        <div class="d-flex w-100 justify-content-between">
                            <img class="rounded mr-3" src="{{ $Visit->Sender->photos['profile']->image }}" style="width: 3em">
                            <h5 class="mb-1">Visited by {{ $Visit->Sender->login }}</h5>
                            <small>{{ $Visit->formatedDate }}</small>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="col-md-6 col-12">
            <div class="list-group mt-3">
                <span class="list-group-item list-group-item-action bg-special-blue text-light mb-3">
                    <div class="d-flex w-100 justify-content-center">
                        <h1 class="mb-0">==== Likes ====</h1>
                    </div>
                </span>
                @foreach($LikeExtendList as $Like)
                    <a href="{{ route('profile.public', ['idProfile' => $Like->Sender->id]) }}" class="list-group-item list-group-item-action bg-special-blue text-light">
                        <div class="d-flex w-100">
                            <img class="rounded mr-3" src="{{ $Like->Sender->photos['profile']->image }}" style="width: 3em">
                            <h5 class="mb-1">Liked by {{ $Like->Sender->login }}</h5>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection
