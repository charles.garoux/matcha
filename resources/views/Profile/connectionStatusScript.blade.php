<script>
    /* ====================== INITIALISATION ====================== */

    let idUser = {{ $idUser }};
    let connectedElement = document.getElementById("connected");
    let disconnectedElement = document.getElementById("disconnected");

    connectedElement.style.display = "none";
    disconnectedElement.style.display = "none";

    refreshLoginStatus();
    setInterval("refreshLoginStatus();", {{ env("AJAX_FREQUENCY") * 1000 }});

    /* ====================== FUNCTIONS ====================== */

    function setVisibility(visibleElement, hiddenElement) {
        visibleElement.style.display = "inline";
        hiddenElement.style.display = "none";
    }

    function timeConverter(UNIX_timestamp){
        var a = new Date(UNIX_timestamp * 1000);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        // var hour = a.getHours();
        // var min = a.getMinutes();
        // var sec = a.getSeconds();
        var time = date + ' ' + month + ' ' + year;
        return time;
    }

    /**
     * Refresh user login status on the profile page. Need variable in script :
     *  connectedElement : DOM element
     *  disconnectedElement : DOM element
     *  idUser : int
     *
     *  Example :
         let connectedElement = document.getElementById("connected");
         let disconnectedElement = document.getElementById("disconnected");
         let idUser = 24;
     *
     */
    function refreshLoginStatus()
    {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '{{ route('ajax.check.alive', compact('idUser')) }}');
        xhr.onload = function() {
            let response;
            if (xhr.status === 200) {
                response = JSON.parse(xhr.responseText);

                if (response['alive'] === true) {
                    setVisibility(connectedElement, disconnectedElement);
                } else {
                    setVisibility(disconnectedElement, connectedElement);
                    if (response['lastAlive'] > 0)
                        disconnectedElement.innerHTML = "Disconnected (last connection : "+ timeConverter(response['lastAlive']) +')';
                }
            }
            else {
                setVisibility(disconnectedElement, connectedElement);
            }
        };
        xhr.send();
    }
</script>
