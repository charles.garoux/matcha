@extends('includes.template')

@section('contenue')
    <div class="row">
        @foreach($MatchExtendList as $Match)
            <div class="col-lg-6 col-12">
                <div class="card bg-special-blue text-light mt-3">
                    <div class="row no-gutters">
                        <div class="col-md-3">
                            <img src="{{ $Match->Profile->photos['profile']->image }}" class="card-img">
                        </div>
                        <div class="col-md-9">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-7">
                                        <h5 class="card-title">{{ $Match->Profile->login }}</h5>
                                        <p class="card-text text-break text-truncate">{{ $Match->Profile->biography }}</p>
                                    </div>
                                    <div class="col-5">
                                        <div class="btn-group-vertical">
                                            <a href="{{ route('profile.public', [ 'idProfile' =>$Match->Profile->id]) }}" class="btn btn-primary">See profile</a>
                                            <a href="{{ route('chat', ['idContact' => $Match->Profile->id]) }}" class="btn btn-love">Open chat</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
