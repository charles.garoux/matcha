<script>
    /* ====================== INITIALISATION ====================== */

    let notificationListElement = document.getElementById("notificationList");
    let bellElement = document.getElementById("notificationBell");
    let buttonElement = document.getElementById("notificationButton");

    keepAlive();
    setInterval("keepAlive();", {{ env("AJAX_FREQUENCY") * 1000 }});

    /* ====================== FUNCTIONS ====================== */

    function notificationElementComponent(notification)
    {
        var element = "" +
            "<button type=\"button\" class=\"btn btn-block btn-love\" onclick=\"clickNotification("+ notification['id'] +")\" \">" +
            ""+ notification['message'] +"" +
            "</button> " +
            "\n";

        return (element);
    }

    /**
     * Flag the notification as view and if it contains a link: redirect to this link
     */
    function clickNotification(idNotification) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '{{ env('APP_URL') }}/notification/'+ idNotification);
        xhr.onload = function() {
            let response;
            if (xhr.status === 200) {
                response = JSON.parse(xhr.responseText);
                if (response['status'] === 'OK') {
                    if (response['notification']['link'] != null && response['notification']['link'].trim() !== '') {
                        window.location = response['notification']['link'];
                    }
                }
            }
        };
        xhr.send();
    }

    /**
     * Tell the server that the user is still connected. Need variable in script :
     *  notificationListElement : DOM element
     *
     *  Example :
        let notificationListElement = document.getElementById("notificationList");
     *
     */
    function keepAlive()
    {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '{{ route('ajax.keep.alive') }}');
        xhr.onload = function() {
            let response;
            if (xhr.status === 200) {
                response = JSON.parse(xhr.responseText);
                if (response['status'] === 'OK' && response['notifications'] !== 'Empty') {
                    var notificationElementsList = "";
                    response['notifications'].forEach(function (value, key, list) {
                        notificationElementsList = notificationElementsList + notificationElementComponent(list[key]);
                    });
                    if (bellElement.dataset.bellStatus !== "ring") {
                        bellElement.src = bellElement.dataset.bellDir +"/bell-ring.svg";
                        bellElement.dataset.bellStatus = "ring";
                    }
                    buttonElement.disabled = false;
                } else {
                    if (bellElement.dataset.bellStatus !== "pause") {
                        bellElement.src = bellElement.dataset.bellDir +"/bell.svg";
                        bellElement.dataset.bellStatus = "pause";
                    }
                    buttonElement.disabled = true;
                    notificationElementsList = "";
                }
            } else {
                notificationElementsList = "An error in the notification occurred";
            }
            notificationListElement.innerHTML = notificationElementsList;
        };
        xhr.send();
    }
</script>
