<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <span class="navbar-brand h1">{{ env("APP_NAME") }}</span>
    @if(Session::has("user"))
        <div class="d-flex flex-row order-2 order-lg-3">
                {{-- Notifications --}}
                <div class="mr-2">
                    {{-- Notifications button --}}
                    <button id="notificationButton" type="button" class="btn btn-love" data-toggle="modal" data-target="#notificationModal" disabled>
                        <img id="notificationBell" data-bell-dir="/asset/img" data-bell-status="pause" src="/asset/img/bell.svg">
                    </button>
                    {{-- Notifications modal --}}
                    <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content bg-special-blue text-light">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="notificationModalTitle">Notifications to check</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <img src="/asset/img/close.svg">
                                    </button>
                                </div>
                                <div id="notificationList" class="modal-body">
                                    {{-- Notifications list --}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                {{-- User actions --}}
                <div class="dropdown">
                    <button class="btn btn-light dropdown-toggle bg-special-blue text-light" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Session::get('user.login') }}
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('account.edit.information') }}">My account</a>
                        <a class="dropdown-item" href="{{ route('profile.public', ['idProfile' => Session::get('user.id')]) }}">My public profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item bg-danger rounded-sm" href="{{ route('disconnect') }}">Disconnect</a>
                    </div>
                </div>
            <button class="navbar-toggler ml-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse order-3 order-lg-2" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                @if(route('suggestion.default') . '/' == (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]")
                    <li class="nav-item active">
                        <a class="nav-link font-weight-bold" href="{{ route('suggestion.default') }}">Suggestion</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('suggestion.default') }}">Suggestion</a>
                    </li>
                @endif

                @if(route('profile.match.list') == (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]")
                    <li class="nav-item active">
                        <a class="nav-link font-weight-bold" href="{{ route('profile.match.list') }}">Match</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('profile.match.list') }}">Match</a>
                    </li>
                @endif

                @if(route('profile.visit.list') == (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]")
                    <li class="nav-item active">
                        <a class="nav-link font-weight-bold" href="{{ route('profile.visit.list') }}">Visitors & Likes</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('profile.visit.list') }}">Visitors & Likes</a>
                    </li>
                @endif

            </ul>
        </div>
    @endif
</nav>
