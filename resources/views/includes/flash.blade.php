@if (Session::has("flash.Success"))
    <div class="alert alert-success" role="alert">
        <li>{{ Session::getFlash("Success") }}</li>
    </div>
@endif
@if (Session::has("flash.Error"))
    <div class="alert alert-danger" role="alert">
        <li>{{ Session::getFlash("Error") }}</li>
    </div>
@endif
@if (Session::has("flash.Info"))
    <div class="alert alert-info" role="alert">
        <li>{{ Session::getFlash("Info") }}</li>
    </div>
@endif
@if (Session::has("flash.Warning"))
    <div class="alert alert-warning" role="alert">
        <li>{{ Session::getFlash("Warning") }}</li>
    </div>
@endif

