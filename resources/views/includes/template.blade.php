<!DOCTYPE html>
<html>
	<head>
		<title>{{ env("APP_NAME") }}</title>

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Project CSS -->
        <link rel="stylesheet" type="text/css" href="/asset/css/style.css">
        <link rel="stylesheet" type="text/css" href="/asset/css/special-bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/asset/css/extension-bootstrap.css">

        <!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="icon" type="image/png" sizes="96x96" href="/asset/img/favicon-96x96.png">

	</head>
	<body>
    @include('includes.navbar')
    <div class="container-fluid">
        <main>
            @include('includes.flash')
            @yield('contenue')
        </main>
    </div>
    <footer class="text-muted mt-5">
        <hr>
        <div class="container">
            <p class="float-right">
                <a href="">Back to top</a>
            </p>
            <p>chgaroux & vicaster &copy; 2019 Matcha</p>
        </div>
    </footer>
    </body>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    @if(Session::has("user"))
        @include('includes.keepAliveNotificationScript')
    @endif
    @yield('script')
</html>
