@extends('includes.template')

@section('contenue')
    <div class="mt-3 text-center">
        <p class="h1">Login</p>
    </div>

    <form method="POST" class="mt-8 text-center">
        <div class="mx-auto col-lg-3 col-md-4 col-12">
            <div class="mt-3">
                <input type="text" class="form-control" id="login" name="login" placeholder="Login" required>
            </div>

            <div class="mt-3">
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
            </div>
            <div class="mt-3">
                <button class="btn btn-primary mt-3" type="submit">Submit form</button>
            </div>
            <div class="mt-3">
                <a href="{{ route('forgetPassword.processing') }}" class="text-secondary">Forget Password</a>
            </div>
            <div class="mt-3">
                <a href="{{ route('register.form') }}" class="text-secondary">Register</a>
            </div>
        </div>
    </form>
@endsection
