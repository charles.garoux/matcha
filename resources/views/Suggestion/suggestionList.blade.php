@extends('includes.template')

@section('contenue')
{{--  SEARCH BAR  --}}
    <div class="row">
        <div class="card bg-special-blue text-light my-2 mx-3">
            <div class="card-body">
                <p class="card-title h2">Filters</p>
                <form id="filterForm" action="{{route('suggestion.filtered.processing')}}" autocomplete="off">
                    <div class="form-row">
                        {{--  AGE  --}}
                        <div class="col-lg-2 col-md-4 col-sm-12 mb-3">
                            <label for="ageFrom">Age from</label>
                            <div class="input-group">
                                <input type="number" class="form-control" id="ageFrom" name="ageFrom" value="{{ $searchValue['ageFrom']['value'] }}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text">years old</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-12 mb-3">
                            <label for="ageTo">Age to</label>
                            <div class="input-group">
                                <input type="number" class="form-control" id="ageTo" name="ageTo" value="{{ $searchValue['ageTo']['value'] }}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text">years old</span>
                                </div>
                            </div>
                        </div>
                        {{--  SCORE  --}}
                        <div class="col-lg-2 col-md-4 col-sm-12 mb-3">
                            <label for="scoreFrom">Score from</label>
                            <input type="number" class="form-control" id="scoreFrom" name="scoreFrom" value="{{ $searchValue['scoreFrom']['value'] }}" required>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-12 mb-3">
                            <label for="scoreTo">Score to</label>
                            <input type="number" class="form-control" id="scoreTo" name="scoreTo" value="{{ $searchValue['scoreTo']['value'] }}" required>
                        </div>
                        {{--  DISTANCE  --}}
                        <div class="col-lg-2 col-md-4 col-sm-12 mb-3">
                            <label for="distanceFrom">Distance from</label>
                            <div class="input-group">
                                <input type="number" class="form-control" id="distanceFrom" name="distanceFrom" value="{{ $searchValue['distanceFrom']['value'] }}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text">km</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-12 mb-3">
                            <label for="distanceTo">Distance to</label>
                            <div class="input-group">
                                <input type="number" class="form-control" id="distanceTo" name="distanceTo" value="{{ $searchValue['distanceTo']['value'] }}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text">km</span>
                                </div>
                            </div>
                        </div>
                        {{--  COMMON POINTS  --}}
                        <div class="col-lg-3 col-md-4 col-sm-12 mb-3">
                            <label for="commonPointsMin">Common points</label>
                            <div class="input-group">
                                <input type="number" class="form-control" id="commonPointsMin" name="commonPointsMin" value="{{ $searchValue['commonPointsMin']['value'] }}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text">common tags</span>
                                </div>
                            </div>
                        </div>

                        @include('includes.forms.autocomplete',
                        ['div_class' => 'col-lg-5 col-md-4 col-sm-12 mb-3',
                        'inputId' => 'autoCompleteTags',
                        'label' => 'Tags',
                        'dropdownButtonText' => 'Tags list',
                        'dropdownText' => 'Tags searched :',
                        'dataListJson' => $tagsListJson,
                        'inputName' => 'tags',
                        'freeMod' => true,
                        'multiChoice' => true])

                        {{--  SORT REFERENCE ATTRIBUTE  --}}
                        <div class="col-lg-4 col-md-4 col-sm-12 mb-3">
                            <label for="sortReferenceAttribute">Sort reference attribute</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="sortReferenceAttribute">The</label>
                                </div>
                                <select class="custom-select" id="sortReferenceAttribute" name="sortReferenceAttribute">
                                    <option value="{{ $searchValue['sortReferenceAttribute']['value'] }}" selected>{{ $searchValue['sortReferenceAttribute']['value'] }}</option>
                                    @foreach($sortReferenceAttributeList as $attribut)
                                        @if($attribut != $searchValue['sortReferenceAttribute']['value'])
                                            <option value="{{ $attribut }}">{{ $attribut }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                    <label class="input-group-text" for="sortReferenceAttribute">attribute</label>
                                </div>
                            </div>
                        </div>

                        {{--  ORDER  --}}
                        <div class="col-lg-3 col-md-4 col-sm-12 mb-3">
                            <label for="order">Order by</label>
                            <div class="input-group mb-3">
                                <select class="custom-select" id="order" name="order">
                                    <option selected value="{{ $searchValue['order']['value'] }}">{{ $searchValue['order']['value'] }}</option>
                                    @foreach($orderList as $attribut)
                                        @if($attribut != $searchValue['order']['value'])
                                            <option value="{{ $attribut }}">{{ $attribut }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                    <label class="input-group-text" for="order">order</label>
                                </div>
                            </div>
                        </div>

                    </div>



                    <button id="searchButton" type="button" class="btn btn-primary btn-block">
                        <span id="searchText">Search</span>
                        <div id="searchSpinner" class="spinner-border" role="status" style="display: none;">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </button>

                </form>
            </div>
        </div>
    </div>

{{--  PROFILES LIST  --}}
    <div class="row mt-3">
        @foreach($profilesList as $Profile)
            <div class="col-xlg-2 col-lg-3 col-md-4 col-sm-6 col-12 my-3">
                <div class="card bg-special-blue text-light">
                    <img src="{{ $Profile->photos['profile']->image }}" class="card-img-top" alt="{{ $Profile->login }}'s profile picture">
                    <div class="card-body">
                        <h5 class="card-title">{{ $Profile->login }}</h5>
                        <span class="badge badge-success">{{ $Profile->score }} points</span>
                        <span class="badge badge-danger">{{ ($Profile->compatibility < 0 ? 0 : $Profile->compatibility) }} points</span>
                        <span class="badge badge-info">{{ round($Profile->distance / 1000, 1) ."km" }}</span>
                        <span class="badge badge-warning">{{ $Profile->commonPoints}} commonPoints</span>
                        <a href="{{ route('profile.public', [ 'idProfile' =>$Profile->id]) }}" class="stretched-link"></a>
                    </div>
                </div>
            </div>
        @endforeach()
    </div>
@endsection

@section('script')
<script>
    document.querySelector("#searchButton").addEventListener("click", search);
    let searchSpinner = document.getElementById('searchSpinner');
    let searchText = document.getElementById('searchText');
    let searchButton = document.getElementById('searchButton');
    let filterForm = document.getElementById('filterForm');

    function search(self) {
        searchSpinner.style.display = '';
        searchText.style.display = 'none';
        searchButton.disabled = true;
        filterForm.submit();
    }
</script>
@endsection
