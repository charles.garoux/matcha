<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
<script src="https://unpkg.com/leaflet.tilelayer.fallback@1.0.4/dist/leaflet.tilelayer.fallback.js"></script>

<script>

    /** Fonction d'initialisation de la carte.
     * A besoin de la latitude (lat) et de la longitude (lon).
     * exemple de div a faire pour la faire fonctionner: "<div id="myMap" style="height: 300px; width: 300px;"></div>"
     * le champ ray est optionnel, il permet d'afficher un cercle de rayon "ray" autour du point.
     */
    function initMap(lat, lon, ray = 0)
    {
        // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
        macarte = L.map('myMap').setView([lat, lon], 6);

        // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
        L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
            minZoom: 3,
            maxZoom: 15,
        }).addTo(macarte);

        // Nous ajoutons un marqueur
        var marker = L.marker([lat, lon]).addTo(macarte);

        // Pour rajouter un rayon
        if (ray != 0)
        {
            if (ray < 0)
                ray = ray * -1;
            var influence = L.circle([lat, lon], ray).addTo(macarte);
        }
    }

    /**
     * Recupere la position par le navigateur et affiche la map.
     * exemple de div a faire pour la faire fonctionner: "<div id="myMap" style="height: 300px; width: 300px;"></div>"
     *      <div id="myMap" class="container" style="height: 40em"></div>
     */
    function getMyPosition(success)
    {
        window.navigator.geolocation.getCurrentPosition(function successPosition(position)
        {
            // Localisation par navigateur
            let lat = position.coords.latitude;
            let lon = position.coords.longitude;
            success(lat, lon);
        }, function errorPosition(error)
        {
            document.getElementById("myMap").innerHTML = "time out";
        }, {maximumAge:10000, timeout: 20000});
    }

</script>
