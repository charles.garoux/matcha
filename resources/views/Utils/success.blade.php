@extends('includes.template')

@section('contenue')
    <div class="row">
        <div class="col-12 text-center">
            <p class="mt-6 h2 text-success">{{ $message }}</p>
        </div>
    </div>
@endsection
