@extends('includes.template')

@section('contenue')

    <div class="mt-3 text-center">
        <p class="h1" >Account information</p>
    </div>

    <form method="POST" autocomplete="off">
        <div class="row">
            <div class="col-sm-6 col-12">
                <div class="mt-3">
                    <label>First name</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $User->first_name }}" required>
                </div>
                <div class="mt-3">
                    <label>Last name</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $User->last_name }}" required>
                </div>
                <div class="mt-3">
                    <label>Login</label>
                    <input type="text" class="form-control" id="login" name="login" value="{{ $User->login }}" required>
                </div>
                <div class="mt-3">
                    <label>Email</label>
                    <div class="input-group">
                        <input type="email" class="form-control" id="email" name="email" value="{{ $User->email }}" required>
                    </div>
                </div>

            </div>
            <div class="col-sm-6 col-12">
                <div class="mt-3">
                    <label>Age (you must have 18 years old)</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="age" name="age" min="18" value="{{ $User->age }}" required>
                    </div>
                </div>
                <div class="mt-3">
                    <label>Biography</label>
                    <div class="input-group">
                        <textarea class="form-control" id="biography" name="biography" maxlength="140" rows="3" required>{{ $User->biography }}</textarea>
                    </div>
                </div>
                <div class="mt-3">
                    <label>My Gender</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="my_gender" name="my_gender" value="{{ $User->Gender->name }}" disabled="disabled">
                    </div>
                </div>
                <div class="mt-3">
                    <div class="input-group">
                        <a href="{{ route('account.edit.information.password.reset') }}" class="btn btn-block btn-primary">Reset my password</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="mt-3 mx-auto col-md-4 col-12">
                <label>Address</label>
                <input type="text" class="form-control" id="address" name="address">
                <small id="emailHelp" class="form-text text-muted">Be as specific as possible</small>
            </div>
            <div class="col-12 my-3">
                <input type="hidden" id="lat" name="lat" value="">
                <input type="hidden" id="lon" name="lon" value="">
                @if (is_array($User->geoCoordinates))
                        <div id="myMap" class="container rounded" style="height: 30em"></div>
                @else
                    <div class="mt-3 text-center">
                        <div id="myMap" class="container rounded" style="height: 30em; display: none"></div>
                        <p id="mapMessage" class="h4 text-warning">We need your address to display the map</p>
                    </div>
                @endif
            </div>
        </div>


        <div class="row mt-5">
            <div class="col-md-4 col-sm-6 col-12">
                @include('includes.forms.autocomplete',
                ['div_class' => 'mt-3',
                'inputId' => 'autoCompleteGender',
                'label' => 'Modify my Genders',
                'dropdownButtonText' => 'Gender',
                'dataListJson' => $GendersListJson,
                'inputName' => 'newGender',
                'freeMod' => true,
                'multiChoice' => false])
                <small class="form-text text-muted">It does not exist? Press enter to create it</small>

                @include('includes.forms.autocomplete',
                ['div_class' => 'mt-3',
                'inputId' => 'autoCompleteAttracted',
                'label' => 'Add Attraction',
                'dropdownButtonText' => 'Genders',
                'dataListJson' => $GendersListJson,
                'inputName' => 'newAttractions',
                'freeMod' => true,
                'multiChoice' => true])
                <small class="form-text text-muted">It does not exist? Press enter to create it</small>

                @include('includes.forms.autocomplete',
                ['div_class' => 'mt-3',
                'inputId' => 'autoCompleteTags',
                'label' => 'Add Tag',
                'dropdownButtonText' => 'Tag list',
                'dataListJson' => $TagsListJson,
                'inputName' => 'newTags',
                'freeMod' => true,
                'multiChoice' => true])
                <small class="form-text text-muted">It does not exist? Press enter to create it</small>

            </div>

            <div class="mt-3 col-md-4 col-sm-6 col-12">
                <label>My Attracted List</label>
                <div>
                    @foreach($User->attractions as $gender)
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <input type="checkbox" name="updatedAttractions[{{ $gender->id }}]" value="{{ $gender->name }}" checked>
                                </div>
                            </div>
                            <input type="text" class="form-control" value="{{ $gender->name }}" disabled="disabled">
                        </div>
                    @endforeach()
                </div>
            </div>
            <div class="mt-3 col-md-4 col-sm-6 col-12">
                <label>My Tag List</label>
                <div>
                    @foreach($User->tags as $tag)
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <input type="checkbox" name="updatedTags[{{ $tag->id }}]" value="{{ $tag->name }}" checked>
                                </div>
                            </div>
                            <input type="text" class="form-control" value="{{ $tag->name }}" disabled="disabled">
                        </div>
                    @endforeach()
                </div>
            </div>
        </div>
        <button id="submit" class="btn btn-block btn-primary mt-5" type="submit">Update my datas</button>
    </form>
    <hr>
    <div class="my-5 text-center">
        <p id="ManagePhotos" class="h1" >Manage Photos</p>
    </div>
    {{-- MANAGE PHOTOS --}}
    @if($User->photos === 'Empty' || isset($User->photos['profile']) === false)
            <form method="post" action="{{ route('account.add.profile-photo') }}" class="col-12">
                <div class="col-xlg-2 col-lg-3 col-md-4 col-sm-6 col-12 my-3 mx-auto">
                    <div id="addProfilePhoto" class="card">
                        <input name="newProfilePhotoImg" type="hidden" value="">
                        <img id="addPhotoImg" src="/asset/img/image-lg-icon.svg" class="card-img-top bg-success" alt="...">
                        <div class="card-body bg-success text-light">
                            <p class="text-warning h4">Drop a profile photo</p>
                            <button type="submit" class="btn btn-block btn-primary">
                                <img src="/asset/img/upload.svg">
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        @endif


        @if($User->photos !== 'Empty')
            <form method="post" action="{{ route('account.update.photo') }}" class="col-12">
                @if(isset($User->photos['profile']))
                    <div class="row">
                        <div class="col-xlg-2 col-lg-3 col-md-4 col-sm-6 col-12 my-3 mx-auto">
                            <div id="profilePhoto" class="card">
                                <input id="profilePhotoInput" name="newProfilePhotoImg" type="hidden" value="">
                                <img id="profilePhotoImg" src="{{ $User->photos['profile']->image }}" class="card-img-top" alt="...">
                                <div class="card-body bg-primary text-light">
                                    <p class="card-text">Profile photo</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if(empty($User->photos['other']) === false)
                    <div class="row">
                        @foreach($User->photos['other'] as $photo)
                            <div class="col-xlg-2 col-lg-3 col-md-4 col-sm-6 col-12 my-3">
                                <div class="card otherPhoto bg-special-blue">
                                    <img src="{{ $photo->image }}" class="card-img-top" alt="...">
                                    <input name="newOtherPhotoImgs[{{ $photo->id }}]" type="hidden" value="">
                                    <div class="card-body bg-secondary text-light">
                                        <p class="card-text">Other photo</p>
                                        <a type="button" href="{{ route('account.delete.photo', ['idPhoto' => $photo->id]) }}" class="btn btn-danger">Delete</a>

                                    </div>
                                </div>
                            </div>
                        @endforeach()
                    </div>
                @endif

                @if(isset($User->photos['profile']) || empty($User->photos['other']) === false)
                    <button id="addImgButton" type="submit" class="btn btn-block btn-success">Update photos</button>
                @endif
            </form>
        @endif

        @if($User->photos === 'Empty' || count($User->photos['other']) < 4)
            <form method="post" action="{{ route('account.add.photo') }}" class="col-12">
                <div class="col-xlg-2 col-lg-3 col-md-4 col-sm-6 col-12 my-3 mx-auto">
                    <div id="addPhoto" class="card">
                        <input name="newOtherPhotoImg" type="hidden" value="">
                        <img id="addPhotoImg" src="/asset/img/image-lg-icon.svg" class="card-img-top bg-success" alt="...">
                        <div class="card-body bg-success text-light">
                            <p class="text-muted">Drop new photo</p>
                            <button type="submit" class="btn btn-block btn-primary">
                                <img src="/asset/img/upload.svg">
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        @endif

@endsection

@section('script')
    @include('Map.mapScript')
    <script>
        @if(is_array($User->geoCoordinates))
            initMap({{ $User->geoCoordinates['lat'] }}, {{ $User->geoCoordinates['lon'] }});
        @else
            getMyPosition(function success(lat, lon) {
                document.getElementById("myMap").style.display = '';
                document.getElementById('lat').value = lat;
                document.getElementById('lon').value = lon;
                document.getElementById('mapMessage').style.display = 'none';
                initMap(lat, lon);
            });
        @endif
    </script>

    {{-- Manage photos --}}
    <script>
        @if(isset($User->photos['profile']) === false)
            let addProfilePhotoElement = document.getElementById('addProfilePhoto');
            setupDropImgEvent(addProfilePhotoElement);
        @else
            let profilePhotoElement = document.getElementById('profilePhoto');
            setupDropImgEvent(profilePhotoElement);
        @endif

        @if($User->photos === 'Empty' || count($User->photos['other']) < 4)
            let addPhotoElement = document.getElementById('addPhoto');
            setupDropImgEvent(addPhotoElement);
        @endif
        @if($User->photos !== 'Empty')
            let otherPhotoElements = document.getElementsByClassName('otherPhoto');
            for (let item of otherPhotoElements) {
                setupDropImgEvent(item);
            }
        @endif

        function setupDropImgEvent(element)
        {
            let preventDefault = (evt) => {evt.preventDefault()};
            element.addEventListener("dragover", preventDefault);
            element.addEventListener("drop", function(dropEvent){
                dropEvent.preventDefault();
                if (dropEvent.dataTransfer.files.length) {
                    loadFile(element.querySelectorAll('img')[0], element.querySelectorAll('input')[0], dropEvent.dataTransfer);

                }
            });
        }

        function loadFile(imgElement, inputElement, fileItem)
        {
            /*
            ** Check if it is an image
            */
            let fileToCheck = fileItem.files[0];
            let fileReader = new FileReader();

            if (fileToCheck.size > 5852790) {
                alert('File too heavy');
                return ;
            }

            fileReader.onloadend = function(e) {
                var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
                var header = "";
                for(var i = 0; i < arr.length; i++) {
                    header += arr[i].toString(16);
                }

                /*
                ** Check the file signature against known types
                */
                var type = "";
                switch (header) {
                    case "89504e47":
                        type = "image/png";
                        break;
                    case "47494638":
                        type = "image/gif";
                        break;
                    case "ffd8ffe0":
                    case "ffd8ffe1":
                    case "ffd8ffe2":
                    case "ffd8ffe3":
                    case "ffd8ffe8":
                        type = "image/jpeg";
                        break;
                    default:
                        type = "unknown";
                        break;
                }

                if (fileToCheck.type != type) {
                    alert("The file need to be an image or a GIF");
                    return ;
                }
                changeImg(fileToCheck, imgElement, inputElement);
            };

            fileReader.readAsArrayBuffer(fileToCheck);
        }

        const toBase64 = file => new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });

        async function changeImg(file, imgElement, inputElement) {
            const base64Img = await toBase64(file);
            imgElement.src = base64Img;
            inputElement.value = base64Img;
        }
    </script>

@endsection
