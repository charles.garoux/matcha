@extends('includes.template')

@section('contenue')
    <div class="uk-position-center">
        <p style="font-size: 25px;">{{ $message }}</p>
    </div>
@endsection
