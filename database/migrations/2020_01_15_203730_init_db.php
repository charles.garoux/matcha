<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sqlFilePath = env('DB_FILE_PATH');
        if (($sqlScript = file_get_contents($sqlFilePath)) === false) {
            echo "[ERROR] file_get_contents($sqlFilePath) => return false";
            return ;
        }
        DB::unprepared($sqlScript);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
