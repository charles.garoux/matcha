<?php

use App\Libraries\Debug;
use App\Libraries\Entities\Attracted;
use App\Libraries\Entities\Gender;
use App\Libraries\Entities\Like;
use App\Libraries\Entities\Photo;
use App\Libraries\Entities\Tag;
use App\Libraries\Entities\Tagged;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $pathToPhotos = '/var/www/lumen-project/database/seeds/Photos/';
        $nbrUsersToGenerate = rand(500, 1000);
        set_time_limit($nbrUsersToGenerate);
        $faker = Faker\Factory::create('fr_FR');
        $tagsList = Tag::listAll();
        $genderList = Gender::listAll();
        $userList = User::listAll();

        $genderRandList = array('female', 'male');
        $ethnicityRandList = array('white', 'black');

        for ($numUsers = 0; $numUsers < $nbrUsersToGenerate; $numUsers++) {
            $userList = User::listAll();
            $physicalFeatures = array();

            $user['idGender'] = $genderList[array_rand($genderList)]->id;
            if ($user['idGender'] == 1)
                $gender = 'female';
            else if ($user['idGender'] == 2)
                $gender = 'male';
            else
                $gender = null;

            array_push($physicalFeatures, ($gender == null? $genderRandList[rand(0,1)] : $gender));
            array_push($physicalFeatures, $ethnicityRandList[array_rand($ethnicityRandList)]);

            $user['age'] = rand(18, 60);
            if ($user['age'] < 30)
                $age = 'young-adult';
            else if ($user['age'] < 50)
                $age = 'adult';
            else
                $age = 'elderly';

            array_push($physicalFeatures, $age);

            $endOfPath = implode('/', $physicalFeatures);

            $photosList = scandir($pathToPhotos.$endOfPath);
            unset($photosList[0]);
            unset($photosList[1]);

            /**
             * Get the profile photo
             */
            $photoPath = $pathToPhotos.$endOfPath .'/'. $photosList[array_rand($photosList)];
            $rawPhoto = file_get_contents($photoPath);
            $base64Photo = base64_encode($rawPhoto);

            /**
             * Define the rest of the attributes
             */
            $user['last_name'] = $faker->lastName;
            $user['first_name'] = $faker->firstName($gender);
            $user['email'] = $faker->unique()->email;
            $user['login'] = $user['first_name'] . $faker->randomNumber(2);
            $user['password'] = Hash::make('qwerty');
            $user['position'] = serialize(array('lat' => $faker->latitude(41, 49), 'lon' => $faker->longitude(-1.8, 8)));
            $user['biography'] = $faker->text(140);
            $idUser = User::insert($user);

            /**
             * Profiles photos
             */
            $nbrPhotos = rand(1, 4);
            Photo::insert(['image' => 'data:image/png;base64,'.$base64Photo, 'profilePhoto' => true, 'idUser' => $idUser]);
            for ($i = 0; $i < $nbrPhotos; $i++) {
                Photo::insert(['image' => 'data:image/png;base64,'.$base64Photo, 'profilePhoto' => false, 'idUser' => $idUser]);
            }


            /**
             * User liked
             *  (to generate score)
             */
            if ($userList != ReturnCase::Empty) {
                $nbrLikes = rand(0, 100);
                for ($i = 0; $i < $nbrLikes; $i++) {
                    $idLiker = $userList[array_rand($userList)]->id;
                    Like::insert(['idTarget' => $idUser, 'idSender' => $idLiker]);
                }
            }

            /**
             * User attract
             */
            $nbrAttract = rand(1, 2);
            for ($i = 0; $i < $nbrAttract; $i++) {
                $idGender = $genderList[array_rand($genderList)]->id;;
                if (Attracted::insert(['idUser' => $idUser, 'idGender' => $idGender]) === false)
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "Attracted::insert(['idUser' => $idUser, 'idGender' => $idGender ]) === false");
            }

            /**
             * User tagging
             */
            $nbrTags = rand(3, 6);
            for ($i = 0; $i < $nbrTags; $i++) {
                $idTag = $tagsList[array_rand($tagsList)]->id;
                if (Tagged::insert(['idUser' => $idUser, 'idTag' => $idTag]) === false)
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "Tagged::insert(['idUser' => $idUser, 'idTag' => $idTag ]) === false");
            }
        }
    }
}
