<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//$router->get('/', ['as' => 'home', 'uses' => 'Controller@welcome'] );

$router->get('/login', ['as' => 'login.form', 'uses' => 'LoginController@form']);
$router->post('/login', ['as' => 'login.processing', 'uses' => 'LoginController@loginProcessing']);

$router->get('/forget-password', ['as' => 'forgetPassword.form', 'uses' => 'ForgetPasswordController@form']);
$router->post('/forget-password', ['as' => 'forgetPassword.processing', 'uses' => 'ForgetPasswordController@resetPassword']);


$router->get('/modify-password/{token}', ['as' => 'forgetPassword.validation.processing', 'uses' => 'ForgetPasswordController@tokenValidation']);
$router->post('/modify-password/{token}', ['as' => 'forgetPassword.Modify.processing', 'uses' => 'ForgetPasswordController@modifyPassword']);

$router->get('/register', ['as' => 'register.form', 'uses' => 'RegisterController@registerForm']);
$router->post('/register', ['as' => 'register.processing', 'uses' => 'RegisterController@register']);

$router->get('/confirm_mail/{token}', ['as' => 'register.confirm.processing', 'uses' => 'RegisterController@isConfirmed']);

/**
 * For routes in this middlewareFor routes in this middleware, user authentication will be verified
 */
$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/disconnect', ['as' => 'disconnect', 'uses' => 'DisconnectController@signOut']);

    /* ================== account completion ================== */
    $router->get('/account/edit/information', ['as' => 'account.edit.information', 'uses' => 'AccountController@editInformations']);
    $router->post('/account/edit/information', ['as' => 'account.edit.information.processing', 'uses' => 'AccountController@editInformationsProcessing']);
    $router->get('/account/edit/information/reset-password', ['as' => 'account.edit.information.password.reset', 'uses' => 'AccountController@changePassword']);

    $router->get('/account/edit/information#ManagePhotos', ['as' => 'account.edit.information.manage-photos', 'uses' => 'AccountController@editInformations']);
    $router->post('/account/update-photos', ['as' => 'account.update.photo', 'uses' => 'AccountController@updateImg']);
    $router->post('/account/update-photos', ['as' => 'account.update.photo', 'uses' => 'AccountController@updateImg']);
    $router->post('account/add-photo', ['as' => 'account.add.photo', 'uses' => 'AccountController@addImg']);
    $router->post('account/add-profile-photo', ['as' => 'account.add.profile-photo', 'uses' => 'AccountController@addProfileImg']);
    $router->get('account/delete-photo-{idPhoto}', ['as' => 'account.delete.photo', 'uses' => 'AccountController@deleteImg']);


    /**
     * For routes in this middleware, if the user account is complete
     */
    $router->group(['middleware' => 'accountComplete'], function () use ($router) {

        /* ================== suggestion ================== */
        $router->get('/', ['as' => 'suggestion.default', 'uses' => 'SuggestionController@suggestion'] );
        $router->get('/suggestion', ['as' => 'suggestion.filtered.processing', 'uses' => 'SuggestionController@filterSuggestion'] );

        /* ================== public profile ================== */
        $router->group(['prefix' => 'profile-{idProfile}'], function () use ($router) {
            $router->get('/', ['as' => 'profile.public', 'uses' => 'ProfileController@show'] );
            $router->get('/like', ['as' => 'profile.public.like.btn', 'uses' => 'ProfileController@likeBtn'] );
            $router->get('/report', ['as' => 'profile.public.report.btn', 'uses' => 'ProfileController@reportBtn'] );
            $router->get('/block', ['as' => 'profile.public.block.btn', 'uses' => 'ProfileController@blockBtn'] );
        });

        /* ================== match ================== */
        $router->get('/my-match', ['as' => 'profile.match.list', 'uses' => 'MatchController@listing'] );

        /* ================== visit ================== */
        $router->get('/my-visitors', ['as' => 'profile.visit.list', 'uses' => 'VisitController@listing'] );

        /* ================== chat ================== */
        $router->get('/chat-{idContact}', ['as' => 'chat', 'uses' => 'ChatController@init'] );


    });
});

/**
 * For routes of AJAX requests in this middleware, user authentication will be verified
 */
$router->group(['middleware' => 'authAjax'], function () use ($router) {
	$router->get('/{idUser}/is-log', ['as' => 'ajax.check.alive', 'uses' => 'RealtimeController@isAlive']);
	$router->get('/keepAlive', ['as' => 'ajax.keep.alive', 'uses' => 'RealtimeController@keepAlive']);
	$router->get('/notification/{idNotification}', ['as' => 'ajax.get.notification', 'uses' => 'RealtimeController@getNotification']);
	$router->get('/chat/{idContact}', ['as' => 'ajax.get.chat', 'uses' => 'RealtimeController@getChat']);
	$router->post('/chat/send-to/{idTarget}', ['as' => 'ajax.send.message', 'uses' => 'RealtimeController@sendMessage']);
});

/* ================== ROUTE DE TEST ================== */
$router->get('/testLib', 'TestLibController@exemple');
