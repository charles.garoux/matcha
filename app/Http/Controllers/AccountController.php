<?php


namespace App\Http\Controllers;


use App\Libraries\Debug;
use App\Libraries\Entities\Attracted;
use App\Libraries\Entities\Gender;
use App\Libraries\Entities\Photo;
use App\Libraries\Entities\ResetTicket;
use App\Libraries\Entities\Tag;
use App\Libraries\Entities\Ticket;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Enums\LogicalOperator;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Position;
use App\Libraries\Utils\Session;
use App\Libraries\Utils\Validate;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AccountController extends Controller
{

    public function editInformations()
    {

        $GendersListJson = "Empty";
        $TagsListJson = "Empty";

        try {
            $User = new User(['id' => $_SESSION['user']['id']]);
        } catch (Exception $e) {
            if ($e->getCode() == 100) {
                $errorMessageToUser = "Error from the DB";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], 'new User() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 101) {
                $errorMessageToUser = "Searched entity doesn't exist";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], 'new User() === false', $errorMessageToUser);
            }
        }
        try {
            $GendersListJson = Gender::listInJson();
        } catch (Exception $e) {
            if ($e->getCode() == 100) {
                $errorMessageToUser = "Error from the DB";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], 'Gender::listInJson() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 101) {
                $errorMessageToUser = "Searched entity doesn't exist";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], 'Gender::listInJson() === false', $errorMessageToUser);
            }
        }

        try {
            $TagsListJson = Tag::listInJson();
        } catch (Exception $e) {
            if ($e->getCode() == 100) {
                $errorMessageToUser = "Error from the DB";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], 'Tag::listInJson() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 101) {
                $errorMessageToUser = "Searched entity doesn't exist";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], 'Tag::listInJson() === false', $errorMessageToUser);
            }
        }

        return view('User.editInformations', ['User' => $User, 'GendersListJson' => $GendersListJson, 'TagsListJson' => $TagsListJson]);
    }


    public function editInformationsProcessing(Request $request)
    {
        $GendersListJson = "Empty";
        $TagsListJson = "Empty";

        try {
            $GendersListJson = Gender::listInJson();
        } catch (Exception $e) {
            if ($e->getCode() == 100) {
                $errorMessageToUser = "Error from the DB";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], 'Gender::listInJson() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 101) {
                $errorMessageToUser = "Searched entity doesn't exist";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], 'Gender::listInJson() === false', $errorMessageToUser);
            }
            else
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], 'Gender::listInJson() === false', 'Unknown error');
        }

        try {
            $TagsListJson = Tag::listInJson();
        }
        catch (Exception $e) {
            if ($e->getCode() == 100) {
                $errorMessageToUser = "Error from the DB";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], 'Tag::listInJson() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 101) {
                $errorMessageToUser = "Searched entity doesn't exist";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], 'Tag::listInJson() === false', $errorMessageToUser);
            }
            else
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 11, __FUNCTION__], 'Tag::listInJson() === false', 'Unknown error');
        }

        try {
            $User = new User(['id' => $_SESSION['user']['id']]);
        }
        catch (Exception $e) {
            if ($e->getCode() == 100) {
                $errorMessageToUser = "Error from the DB";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], 'Tag::listInJson() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 101) {
                $errorMessageToUser = "Searched entity doesn't exist";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], 'Tag::listInJson() === false', $errorMessageToUser);
            }
            else
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 11, __FUNCTION__], 'Tag::listInJson() === false', 'Unknown error');
        }

        if (($errorMessageToUser = $this->updateAttractions($request, $User)) !== true) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' \$this->updateAttractions($request, $User) !== true', $errorMessageToUser);
        }
        if (($errorMessageToUser = $this->updateTags($request, $User)) !== true) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' \$this->updateTags($request, $User) !== true', $errorMessageToUser);
        }
        if (($errorMessageToUser = $this->updateGender($request, $User)) !== true) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' \$this->updateGender($request, $User) !== true', $errorMessageToUser);
        }
        if (($errorMessageToUser = $this->addAttraction($request, $User)) !== true) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' \$this->addAttraction($request, $User) !== true', $errorMessageToUser);
        }
        if (($errorMessageToUser = $this->addTag($request, $User)) !== true) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' \$this->addTag($request, $User) !== true', $errorMessageToUser);
        }
        if (($errorMessageToUser = $this->updateLocation($request, $User)) !== true) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' \$this->updateLocation($request, $User) !== true', $errorMessageToUser);
        }

        $pos_bool = false;
        $pos = [];
        $inputsList = [//replacer les $_POST par request->input
            'first_name' => ['value' => ($_POST['first_name'] ?? NULL), 'type' => 'str'],
            'last_name' => ['value' => ($_POST['last_name'] ?? NULL), 'type' => 'str'],
            'login' => ['value' => ($_POST['login'] ?? NULL), 'type' => 'login'],
            'email' => ['value' => ($_POST['email'] ?? NULL), 'type' => 'email'],
            'age' => ['value' => ($_POST['age'] ?? NULL), 'type' => 'int'],
            'biography' => ['value' => ($_POST['biography'] ?? NULL), 'type' => 'str']
        ];

        /**
         * check si l'inputlist est conforme
         */
        if (($errorMessageToUser = Validate::check($inputsList)) !== true) {
            if ($errorMessageToUser == "Login too short (3 characters minimum)" || $errorMessageToUser == "Login too long (32 characters maximum)") {
                Session::flash('Warning', "Your login is not conform. Between 3 and 32 characters.");
                return view('User.editInformations', ['User' => $User, 'GendersListJson' => $GendersListJson, 'TagsListJson' => $TagsListJson]);
            }
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 5, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
        }

        /**
         * verification si le changement d'adresse doit se faire
         */
        if (empty($_POST['address']) === false) {
            $address = ['address' => ['value' => $_POST['address'], 'type' => 'str']];
            if (($errorMessageToUser = Validate::check($address)) !== true) {
                return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
            }
            if (($pos = Position::getByAdress($address['address']['value'])) === false) {
                Session::flash('Warning', "Invalid address");
                return view('User.editInformations', ['User' => $User, 'GendersListJson' => $GendersListJson, 'TagsListJson' => $TagsListJson]);
            }
            $pos_bool = true;
        }

        /**
         * check si le nouveau login existe deja:
         */
        if (($DB_User = User::getby('login', $inputsList['login']['value'])) !== false) {
            if ($DB_User !== "Empty") {
                if ($DB_User->id !== $User->id) {
                    Session::flash('Warning', "Login allready used");
                    return view('User.editInformations', ['User' => $User, 'GendersListJson' => $GendersListJson, 'TagsListJson' => $TagsListJson]);
                }
            }
        }
        else {
            $errorMessageToUser = "There is an error with the database";
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 10, __FUNCTION__], 'User::getByExtend() === false', $errorMessageToUser);
        }

        /**
         * check si le nouvel email existe deja:
         */
        if (($DB_User = User::getby('email', $inputsList['email']['value'])) !== false) {
            if ($DB_User !== "Empty") {
                if ($DB_User->id !== $User->id) {
                    Session::flash('Warning', "Email allready used");
                    return view('User.editInformations', ['User' => $User, 'GendersListJson' => $GendersListJson, 'TagsListJson' => $TagsListJson]);
                }
            }
        }
        else {
            $errorMessageToUser = "There is an error with the database";
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 10, __FUNCTION__], 'User::getByExtend() === false', $errorMessageToUser);
        }

        /**
         * ecrasement des anciennes donnees par les nouvelles
         */
        $User->first_name = $inputsList['first_name']['value'];
        $User->last_name = $inputsList['last_name']['value'];
        $User->login = $inputsList['login']['value'];
        $User->email = $inputsList['email']['value'];
        $User->age = $inputsList['age']['value'];
        $User->biography = $inputsList['biography']['value'];
        if ($pos_bool === true) {
            $User->position = serialize($pos);
        }
        try {
            $User->save();
        }
        catch (Exception $e) {
            if ($e->getCode() == 100) {
                $errorMessageToUser = "Error from the DB";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], 'Tag::listInJson() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 101) {
                $errorMessageToUser = "Searched entity doesn't exist";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], 'Tag::listInJson() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 102) {
                $errorMessageToUser = "Error from the DB, can't update";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 13, __FUNCTION__], '$User->save() === false', $errorMessageToUser);
            }
        }
        $_SESSION['user']['login'] = $User->login;
        return redirect(route('account.edit.information'));
    }

    /* ======================= MANAGE GENDER ======================= */

    private function updateGender(Request $request, User $User)
    {
        $inputsList = [
            'newGender' => ['value' => ($request->input('newGender') ?? $User->Gender->name), 'type' => 'str'],
        ];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            return ($errorMessage);
        }
        $newGender = $inputsList['newGender']['value'];
        if ($newGender === $User->Gender->name) {
            return (true);
        }

        if (($genderFound = Gender::getby('name', $newGender)) === false){
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' Gender::getby(\'name\', $newGender) === false');
            return ('Error from the DB');
        }
        else if ($genderFound === ReturnCase::Empty) {
            if (($idInsertGender = Gender::insert(['name' => $newGender])) === false) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' Gender::insert([\'name\' => $newGender]) === false');
                return ('Error from the DB');
            }
        }

        $idNewGender = ($idInsertGender ?? $genderFound->id);

        $User->Gender->id = $idNewGender;
        $User->Gender->name = $newGender;
        try {
            $User->save();
        } catch (Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' Gender::insert([\'name\' => $newGender]) === false');
            return ('Error from the DB during update');
        }

        return (true);
    }

    /* ======================= ADD ATTRACTION AND TAGS ======================= */

    private function addAttraction(Request $request, User $User)
    {
        $inputsList = [
            'newAttractions' => ['value' => ($request->input('newAttractions') ?? []), 'type' => 'array', 'arrayKeyType' => 'forceInt', 'arrayValueType' => 'str'],
        ];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            return ($errorMessage);
        }
        $newAttractions = $inputsList['newAttractions']['value'];

        if (($errorMessage = $this->addExternalLink($User, 'attraction', $newAttractions, 'Attracted', 'Gender')) !== true) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], '$this->addExternalLink($User, \'attraction\', '. json_encode($newAttractions) .', \'Attracted\', \'Gender\') !== true)');
            return ($errorMessage);
        }

        return (true);
    }

    private function addTag(Request $request, User $User)
    {
        $inputsList = [
            'newTags' => ['value' => ($request->input('newTags') ?? []), 'type' => 'array', 'arrayKeyType' => 'forceInt', 'arrayValueType' => 'str'],
        ];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            return ($errorMessage);
        }
        $newTags = $inputsList['newTags']['value'];

        if (($errorMessage = $this->addExternalLink($User, 'tags', $newTags, 'Tagged', 'Tag')) !== true) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], '$this->addExternalLink($User, \'attraction\', '. json_encode($newTags) .', \'Attracted\', \'Gender\') !== true)');
            return ($errorMessage);
        }

        return (true);
    }

    /**
     * @param User $User
     * @param string $externalLinkName : 'tags' or 'attractions' (User attribut)
     * @param string $className 'Tagged' or 'Attracted'
     * @param string $elementName 'Tag' or 'Gender'
     * @param array $newExternalLink
     * @return bool|string
     */
    private function addExternalLink(User $User, string $externalLinkName, array $newExternalLink, string $className, string $elementName)
    {
        $class = "\App\Libraries\Entities\\$className";
        $elementClass = "\App\Libraries\Entities\\$elementName";

        foreach ($newExternalLink as $key => $newElementName) {
            if (isset($User->$externalLinkName[$newElementName]))
                unset($newExternalLink[$key]);
        }

        foreach ($newExternalLink as $newElementName) {
            if (($element = $elementClass::getBy('name', $newElementName)) === false) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $elementClass::getBy('name', $newElementName) === false");
                return ("An error has occurred during search the $newElementName in database");
            }

            if ($element === ReturnCase::Empty) {
                if (($idNewElement = $elementClass::insert(['name' => $newElementName])) === false) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $elementClass::insert(['name' => $newElementName]) === false");
                    return ("An error has occurred during insert the $newElementName in database");
                }
            }

            $idElement = ($idNewElement ?? $element->id);

            if (($class::insert(['id'.$elementName => $idElement, 'idUser' => $User->id])) === false) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $class::insert(['id'.$elementName => $idElement, 'idUser' => $User->id]) === false");
                return ("An error has occurred during insert the $elementName in database");
            }
        }

        return (true);
    }

    /* ======================= MANAGE LATEST ATTRACTIONS AND TAGS ======================= */

    private function updateAttractions(Request $request, User $User)
    {
        $latestAttractions = $User->attractions;

        $inputsList = [
            'updatedAttractions' => ['value' => ($request->input('updatedAttractions') ?? []), 'type' => 'array', 'arrayKeyType' => 'int', 'arrayValueType' => 'str'],
        ];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            return ($errorMessage);
        }
        $updatedAttractionGender = $inputsList['updatedAttractions']['value'];

        if (($errorMessage = $this->updateExternalLink($User->id, 'Attracted', 'Gender', $updatedAttractionGender, $latestAttractions)) !== true) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " \$this->updateExternalLink($User->id, 'Tagged', 'Tag', ". json_encode($updatedAttractionGender) .",". json_encode($latestAttractions) .") !== true)");
            return ($errorMessage);
        }

        return(true);
    }

    private function updateTags(Request $request, User $User)
    {
        $latestTags = $User->tags;

        $inputsList = [
            'updatedTags' => ['value' => ($request->input('updatedTags') ?? []), 'type' => 'array', 'arrayKeyType' => 'int', 'arrayValueType' => 'str'],
        ];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            return ($errorMessage);
        }
        $updatedTags = $inputsList['updatedTags']['value'];

        if (($errorMessage = $this->updateExternalLink($User->id, 'Tagged', 'Tag', $updatedTags, $latestTags)) !== true) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " \$this->updateExternalLink($User->id, 'Tagged', 'Tag', ". json_encode($updatedTags) .",". json_encode($latestTags) .") !== true)");
            return ($errorMessage);
        }

        return(true);
    }


    /**
     * @param int $idUser
     * @param string $className : 'Tagged' or 'Attracted'
     * @param string $elementName : 'Tag' or 'Gender'
     * @param array $updatedElement
     * @param array $latestElement
     * @return bool|string
     */
    private function updateExternalLink(int $idUser, string $className, string $elementName, array $updatedElement, array $latestElement)
    {
        $class = "\App\Libraries\Entities\\$className";

        foreach ($updatedElement as $id => $name) {
            if (isset($latestElement[$name]))
                unset($latestElement[$name]);
        }

        foreach ($latestElement as $element) {
            if ($class::deleteByExtend(['idUser' => $idUser, 'id'.$elementName => $element->id], LogicalOperator::AND) === false) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "$class::deleteByExtend(['idUser' => $idUser, 'id'.$elementName => $element->id], LogicalOperator::AND) === false");
                return ("An error has occurred during delete the $className in database");
            }
        }

        return (true);
    }

    /* ======================= MANAGE PHOTOS ======================= */
    /* add, update and delete */

    public function updateImg(Request $request)
    {
        try {
            $idUser = Session::get('user.id');
            $User = new User(['id' => Session::get('user.id')]);
        } catch (\Exception $e) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
        }
        $newProfilePhoto = $request->input('newProfilePhotoImg');

        /**
         * Fill and sanitize $inputsList
         * (Delete empty image in newOtherPhotoImgs)
         */
        $inputsList = [
            'newOtherPhotoImgs' => ['value' => ($request->input('newOtherPhotoImgs') ?? []), 'type' => 'array', 'arrayKeyType' => 'int', 'arrayValueType' => 'imageBase64'],
        ];
        if (isset($User->photos['profile']) && empty($newProfilePhoto) === false)
            $inputsList += ['newProfilePhotoImg' => ['value' => ($request->input('newProfilePhotoImg') ?? NULL), 'type' => 'imageBase64']];
        foreach ($inputsList['newOtherPhotoImgs']['value'] as $key => $img) {
            if (empty($img))
                unset($inputsList['newOtherPhotoImgs']['value'][$key]);
        }
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessage);
        }

        $photosList = $inputsList['newOtherPhotoImgs']['value'];
        if (isset($inputsList['newProfilePhotoImg'])) {
            $photosList[$User->photos['profile']->id] = $inputsList['newProfilePhotoImg']['value'];
        }

        foreach ($photosList as $id => $newImage) {
            if (empty($newImage))
                unset($photosList[$id]);
        }
        foreach ($photosList as $id => $newImage) {
            if (Photo::update(['image' => $newImage], ['id' => $id, 'idUser' => $User->id], LogicalOperator::AND) === false) {
                $errorMessage = 'Database error when updating a photo';
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ' Photo::update([\'image\' => $newImage], [\'id\' => $id, \'idUser\' => $User->id], LogicalOperator::AND) === false', $errorMessage);
            }
        }

        return redirect(route('account.edit.information.manage-photos'));
    }

    public function addImg(Request $request)
    {
        try {
            $idUser = Session::get('user.id');
            $User = new User(['id' => Session::get('user.id')]);
        } catch (\Exception $e) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
        }
        $cleanBase64 = $request->input('newPhotoImg');

        $inputsList = ['newPhotoImg' => ['value' => ($request->input('newOtherPhotoImg') ?? NULL), 'type' => 'imageBase64']];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessage);
        }
        $base64Img = $inputsList['newPhotoImg']['value'];

        if (($errorMessage = $this->addPhoto($User, $base64Img, false)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], ' $this->addPhoto($User, $base64Img, true) !== true', $errorMessage);
        }

        return redirect(route('account.edit.information.manage-photos'));
    }

    public function addProfileImg(Request $request)
    {
        try {
            $idUser = Session::get('user.id');
            $User = new User(['id' => Session::get('user.id')]);
        } catch (\Exception $e) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
        }

        $inputsList = ['newPhotoImg' => ['value' => ($request->input('newProfilePhotoImg') ?? NULL), 'type' => 'imageBase64']];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessage);
        }
        $base64Img = $inputsList['newPhotoImg']['value'];

        if (($errorMessage = $this->addPhoto($User, $base64Img, true)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], ' $this->addPhoto($User, $base64Img, true) !== true', $errorMessage);
        }

        return redirect(route('account.edit.information.manage-photos'));
    }

    private function addPhoto(User $User, String $base64Img, bool $isProfilePhoto)
    {
        if ($isProfilePhoto && isset($User->photos['profile']) && count($User->photos['profile']) >= 1) {
            Debug::print(SignalType::Warning, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ' count($User->photos[\'other\']) >= 1');
            return ("You have reached the limit of the number of profile photo");
        }
        else if ($isProfilePhoto === false && isset($User->photos['other']) && count($User->photos['other']) >= 4) {
            Debug::print(SignalType::Warning, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ' count($User->photos[\'other\']) >= 4');
            return ("You have reached the limit of the number of photos");
        }

        if (Photo::insert(['image' => $base64Img, 'idUser' => $User->id, 'profilePhoto' => $isProfilePhoto]) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ' Photo::insert([\'image\' => $cleanBase64, \'idUser\' => $User->id, \'profilePhoto\' => false] === false');
            return ("An error has occured during add the new photo in database");
        }

        return (true);
    }

    public function deleteImg(Request $request)
    {
        $inputsList = [
            'idPhoto' => ['value' => ($request->route('idPhoto') ?? NULL),'type' => 'int'],
        ];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessage);
        }

        if (($nbrDeleted = Photo::deleteByExtend(['id' => $request->route('idPhoto'), 'idUser' => Session::get('user.id'), 'profilePhoto' => false], LogicalOperator::AND)) === false) {
            $errorMessageToUser = "An error has occurred during delete the photo in database";
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], 'User::getByExtend() === false', $errorMessageToUser);
        }
        else if ($nbrDeleted === 0) {
            $errorMessageToUser = "You can't delete this photo";
            return Debug::view(SignalType::Warning, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ' $nbrDeleted === 0', $errorMessageToUser);
        }
        return redirect(route('account.edit.information.manage-photos'));
    }

    public function changePassword()
    {
        $UserId = Session::get('user.id');

        try {
        $User = new User(['id' => $UserId]);
    }
    catch (Exception $e) {
        if ($e->getCode() == 100) {
            $errorMessageToUser = "Error from the DB";
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], 'new ResetTicket() === false', $errorMessageToUser);
        }
        if ($e->getCode() == 101) {
            $errorMessageToUser = "Searched entity doesn't exist";
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], 'new ResetTicket() === false', $errorMessageToUser);
        }
    }
        $User = [
            'id' => $User->id,
            'first_name' => $User->first_name,
            'last_name' => $User->last_name,
            'login' => $User->login,
            'password' => $User->password,
            'email' => $User->email
        ];
        $token = Ticket::generator();
        $route = route('forgetPassword.validation.processing', compact('token'));
        $viewData = array('user' => $User, 'appAddress' => $route, 'token' => $token);

        if (ResetTicket::insert(['key' => $token, 'idUser' => $User['id']]) !== false)
        {
            Mail::send('Emails.ModifPassword', $viewData, function($message) use($User)
            {
                $message->to($User['email'])
                    ->subject('Password reset');
            });
            $message = "Please check your email to modify the password.";
            return view('Utils.success', compact('message'));
        }
        else {
            $errorMessageToUser = "There is an error with the database";
            ResetTicket::deleteBy('idUser', $User['id']);
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 11, __FUNCTION__], 'RegisterTicket::insert() === false', $errorMessageToUser);
        }
    }

    private function updateLocation(Request $request, User $User)
    {
        if (empty($request->input('address')) === false)
            return (true);

        if ($User->geoCoordinates === 'Empty') {
            $default = Position::getByIP();
            if ($default === null) //If the IP is local (like virtualBox network)
                $default = Position::getByAdress('11 Passage Panama Lyon');
        }
        else {
            $default['lat'] = $User->geoCoordinates['lat'];
            $default['lon'] = $User->geoCoordinates['lon'];
        }

        if (empty($request->input('lat')) || empty($request->input('lon')))
            $inputsList = [
                'lat' => ['value' => $default['lat'], 'type' => 'float'],
                'lon' => ['value' => $default['lon'], 'type' => 'float']
            ];
        else
            $inputsList = [
                'lat' => ['value' => ($request->input('lat') ?? $default['lat']), 'type' => 'float'],
                'lon' => ['value' => ($request->input('lon') ?? $default['lon']), 'type' => 'float']
            ];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            return ($errorMessage);
        }
        $pos = ['lat' => $inputsList['lat']['value'], 'lon' => $inputsList['lon']['value']];

        if (($User->position = serialize($pos)) === false) {
            Debug::print(SignalType::Error, 'SERIALIZATION', [__FILE__, __LINE__ - 1, __FUNCTION__], " serialize(". json_encode($pos) .") === false");
            return ('Error from the DB during update');
        }

        try {
            $User->save();
        } catch (Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' Gender::insert([\'name\' => $newGender]) === false');
            return ('Error from the DB during update');
        }

        return (true);
    }

}




        /**
         * [ WIP ]
         * modification du genre.
         *
        if (empty($_POST['my_gender']) === false) {
            $my_gender = ['my_gender' => ['value' => $_POST['my_gender'], 'type' => 'str']];

            if (($errorMessageToUser = Validate::check($my_gender)) !== true) {
                return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
            }


            $my_gender = Gender::getBy('name', $my_gender['my_gender']['value']);
            if ($my_gender === "Empty") {
                $my_gender = 3;
            }
            else {
                $my_gender = $my_gender->id;
            }
            $my_gender_bool = 1;
        }
         */
