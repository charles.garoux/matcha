<?php

namespace App\Http\Controllers;

use App\Libraries\Debug;
use App\Libraries\Entities\RegisterTicket;
use App\Libraries\Entities\Ticket;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Enums\LogicalOperator;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;
use App\Libraries\Utils\Validate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use TheSeer\Tokenizer\Token;

class RegisterController extends Controller
{
	public function registerForm()
	{
        return view('Register.register');
    }

    public function mustBeConfirm()
    {
        return view('Register.mustBeConfirm');
    }

    /**
     * @param $token
     *
     * Compare la key de l'url avec la DB pour confirmer le compte.
     * Si c'est confirmé, supprime la key de la DB.
     *
     * Requiert la view "Register.register", "Register.mustBeConfirm"
     *
     * @return \Illuminate\View\View
     */

    public function isConfirmed($token)
    {
        if (($DB_Token = RegisterTicket::getBy('key', $token)) !== false) {
            if ($DB_Token !== "Empty") {
                if (strcmp($DB_Token->key, $token) == 0) {
                    RegisterTicket::deleteBy('key', $DB_Token->key);
                    return view('Register.isConfirmed');
                }
                else {
                    Session::flash('Warning', "Token not good or expired");
                    return view('Register.mustBeConfirm');
                }
            }
            else {
                Session::flash('Warning', "Token not good or expired");
                return view('Register.mustBeConfirm');
            }
        }
        else {
            $errorMessageToUser = "There is an error with the database";
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 18, __FUNCTION__], 'RegisterTicket::getBy() === false', $errorMessageToUser);
        }
    }

    /**
     * @param Request $request
     *
     * Cree l'user, l'insert dans la DB, cree le token et l'insert dans la DB,
     * Puis envoie le mail de confirmation.
     *
     * Requiert la view "Register.register", "Register.mustBeConfirm"
     *
     * @return array|\Illuminate\View\View
     */
    public function register()
    {
        $inputsList = [
            'first_name' => ['value' => ($_POST['first_name'] ?? NULL), 'type' => 'str'],
            'last_name' => ['value' => ($_POST['last_name'] ?? NULL), 'type' => 'str'],
            'login' => ['value' => ($_POST['login'] ?? NULL), 'type' => 'login'],
            'email' => ['value' => ($_POST['email'] ?? NULL), 'type' => 'email'],
            'confirm_email' => ['value' => ($_POST['confirm_email'] ?? NULL), 'type' => 'email'],
            'password' => ['value' => ($_POST['password'] ?? NULL), 'type' => 'str'],
            'confirm_password' => ['value' => ($_POST['confirm_password'] ?? NULL), 'type' => 'str']
        ];
        if (($errorMessageToUser = Validate::check($inputsList)) !== true) {
            if ($errorMessageToUser == "Login too short (3 characters minimum)" || $errorMessageToUser == "Login too long (32 characters maximum)") {
                Session::flash('Warning', "Your login is not conform. Between 3 and 32 characters.");
                return view('Register.register');
            }
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
        }

        if(strcmp($inputsList['password']['value'], $inputsList['confirm_password']['value']) !== 0) {
            Session::flash('Warning', "Your password confirmation is bad");
            return view('Register.register');
        }
        if (strcmp($inputsList['email']['value'], $inputsList['confirm_email']['value']) !== 0) {
            Session::flash('Warning', "Your email confirmation is bad");
            return view('Register.register');
        }

        $user = [
            'first_name' => $inputsList['first_name']['value'],
            'last_name' => $inputsList['last_name']['value'],
            'login' => $inputsList['login']['value'],
            'password' => Hash::make($inputsList['password']['value']),
            'email' => $inputsList['email']['value']
        ];

        if (($oldUser = User::getByExtend(['login' => $inputsList['login']['value'], 'email' => $inputsList['email']['value']], LogicalOperator::OR)) !== false) {
            if ($oldUser !== 'Empty') {
                Session::flash('Warning', "Login or Email allready used");
                return view('Register.register');
            }

            if (($idUser = User::insert($user)) !== false) {
                $user['id'] = $idUser;
                $token = Ticket::generator();
                $route = route('register.confirm.processing', compact('token'));
                $viewData = array('user' => $user, 'appAddress' => $route, 'token' => $token);
                $email = $user['email'];

                if (RegisterTicket::insert(['key' => $token, 'idUser' => $user['id']]) !== false) {
                    Mail::send('Emails.accountValidation', $viewData, function($message) use($email) {
                        $message->to($email)
                            ->subject('Confirm account: Matcha');
                    });
                    return view('Register.mustBeConfirm');
                }
                else {
                    $errorMessageToUser = "There is an error with the database";
                    RegisterTicket::deleteBy('idUser', $user['id']);
                    User::deleteBy('id', $user['id']);
                    return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 11, __FUNCTION__], 'RegisterTicket::insert() === false', $errorMessageToUser);
                }
            }
        }
        else {
            $errorMessageToUser = "There is an error with the database";
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 30, __FUNCTION__], 'User::getByExtend() === false', $errorMessageToUser);
        }
    }
}
