<?php

namespace App\Http\Controllers;

use App\Libraries\Debug;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Auth;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;
use App\Libraries\Utils\Validate;
use Exception;
use Illuminate\Support\Facades\Hash;

/**
 * Class LoginController
 * @package App\Http\Controllers
 *
 * Class permettant la connexion des utilisateurs.
 */
class LoginController extends Controller
{

    /**
     * Simple redirection sur la view 'login'
     *
     * @return \Illuminate\View\View
     */
    public function form()
    {
        return view('Login.login');
    }

    /**
     * Check si $inputList du form est ok
     * puis connect l'utilisateur
     *
     * @return \Illuminate\View\View
     */
    public function loginProcessing()
    {
        $inputsList = [
            'login' => ['value' => ($_POST['login'] ?? NULL), 'type' => 'login'],
            'password' => ['value' => ($_POST['password'] ?? NULL), 'type' => 'str']
        ];
        if (($errorMessageToUser = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
        }

        try {
            Auth::signIn($inputsList['login']['value'], $inputsList['password']['value']);
        }
        catch (Exception $e) {
            if ($e->getCode() == 200) {
                $errorMessageToUser = "There is an error with the database";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 4, __FUNCTION__], 'User::getByExtend() === false', $errorMessageToUser);
            }
            else if ($e->getCode() == 201) {
                Session::flash('Warning', "Unknown login");
                return view('Login.login');
            }
            else if($e->getCode() == 202) {
                Session::flash('Warning', "The password is incorrect");
                return view('Login.login');
            }
            else if($e->getCode() == 203) {
                Session::flash('Warning', "The account has not been validated");
                return view('Login.login');
            }
        }
        return redirect(route('suggestion.default'));
    }
}
