<?php


namespace App\Http\Controllers;


use App\Libraries\Debug;
use App\Libraries\Entities\Tag;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\SearchEngine;
use App\Libraries\Utils\Session;
use App\Libraries\Utils\Validate;
use Illuminate\Http\Request;

class SuggestionController extends Controller
{
    /**
     * Contains default search values
     * @var array
     */
    private $defaultSearchValue = [
        'ageFrom' => ['value' => 20],
        'ageTo' => ['value' => 30],
        'scoreFrom' => ['value' => 0],
        'scoreTo' => ['value' => 100],
        'distanceFrom' => ['value' => 0],
        'distanceTo' => ['value' => 1000],
        'tagNameList' => ['value' => []],
        'commonPointsMin' => ['value' => 0],
        'sortReferenceAttribute' => ['value' => 'compatibility'],
        'order' => ['value' => 'descending'],
    ];

    /**
     * Contains which reference attributes are allowed to sort the search results
     * @var array
     */
    private $sortReferenceAttributeList = [
        'compatibility',
        'score',
        'distance',
        'age',
        'commonPoints'
    ];

    /**
     * Contains which reference attributes are allowed to sort the search results
     * @var array
     */
    private $orderList = [
        'ascending',
        'descending'
    ];

    public function suggestion()
    {
        $idUser = Session::get('user.id');
        $default = $this->defaultSearchValue;
        $tagsListJson = Tag::listInJson();

        try {
            $User = new User(['id' => Session::get('user.id')]);
        } catch (\Exception $e) {
            $errorCode = $e->getCode();
            if ($errorCode != 101)
                return Debug::view(SignalType::Warning, 'DB', [__FILE__, __LINE__ - 4, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
        }

        $userListFound = SearchEngine::findUsers($User,
            $default['ageFrom']['value'],
            $default['ageTo']['value'],
            $default['distanceFrom']['value'],
            $default['distanceTo']['value'] * 1000,
            $default['scoreFrom']['value'],
            $default['scoreTo']['value'],
            $default['tagNameList']['value'],
            $default['commonPointsMin']['value']);
        if ($userListFound === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 10, __FUNCTION__], " SearchEngine::findUsers()");
            $profilesList = [];
            Session::flash('Error', 'An error occurred during the search');
            return view('Suggestion.suggestionList',
                compact('profilesList', 'tagsListJson'),
                ['searchValue' => $default, 'sortReferenceAttributeList'=> $this->sortReferenceAttributeList, 'orderList' => $this->orderList]);
        }

        $profilesList = SearchEngine::sort($userListFound,
            $default['sortReferenceAttribute']['value'],
            $default['order']['value']);

        return view('Suggestion.suggestionList',
            compact('profilesList', 'tagsListJson'),
            ['searchValue' => $default, 'sortReferenceAttributeList'=> $this->sortReferenceAttributeList, 'orderList' => $this->orderList]);
    }

    public function filterSuggestion(Request $request)
    {
        $default = $this->defaultSearchValue;
        $tagsListJson = Tag::listInJson();
        try {
            $User = new User(['id' => Session::get('user.id')]);
        } catch (\Exception $e) {
            $errorCode = $e->getCode();
            if ($errorCode != 101)
                return Debug::view(SignalType::Warning, 'DB', [__FILE__, __LINE__ - 4, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
        }

        $inputsList = [
            'ageFrom' => ['value' => ($request->input('ageFrom') ?? $default['ageFrom']['value']),'type' => 'int'],
            'ageTo' => ['value' => ($request->input('ageTo') ?? $default['ageTo']['value']), 'type' => 'int'],
            'scoreFrom' => ['value' => ($request->input('scoreFrom') ?? $default['scoreFrom']['value']), 'type' => 'int'],
            'scoreTo' => ['value' => ($request->input('scoreTo') ?? $default['scoreTo']['value']), 'type' => 'int'],
            'distanceFrom' => ['value' => ($request->input('distanceFrom') ?? $default['distanceFrom']['value']), 'type' => 'int'],
            'distanceTo' => ['value' => ($request->input('distanceTo') ?? $default['distanceTo']['value']), 'type' => 'int'],
            'commonPointsMin' => ['value' => ($request->input('commonPointsMin') ?? $default['commonPointsMin']['value']), 'type' => 'int'],
            'sortReferenceAttribute' => ['value' => ($request->input('sortReferenceAttribute') ?? $default['sortReferenceAttribute']['value']), 'type' => 'enum', 'enumList' => $this->sortReferenceAttributeList],
            'order' => ['value' => ($request->input('order') ?? $default['order']['value']), 'type' => 'enum', 'enumList' => $this->orderList],
            'tags' => ['value' => ($request->input('tags') ?? []), 'type' => 'array', 'arrayKeyType' => 'int', 'arrayValueType' => 'str'],
        ];
        if (($errorMessage = Validate::check($inputsList)) !== true) {
            Session::flash('Warning', $errorMessage);
            return view('Suggestion.suggestionList',
                compact('tagsListJson'),
                ['profilesList' => [], 'searchValue' => $default, 'sortReferenceAttributeList'=> $this->sortReferenceAttributeList, 'orderList' => $this->orderList]);
        }

        /**
         * Generate variables from the inputs list
         */
        foreach ($inputsList as $varName => $varArray) {
            $$varName = $varArray['value'];
        }



        $userListFound = SearchEngine::findUsers($User,
            $ageFrom,
            $ageTo,
            ($distanceFrom > 0 ? $distanceFrom * 1000 : 0),
            $distanceTo * 1000,
            $scoreFrom,
            $scoreTo,
            $tags,
            $commonPointsMin);
        if ($userListFound === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 10, __FUNCTION__], " SearchEngine::findUsers()");
            $profilesList = [];
            Session::flash('Error', 'An error occurred during the search');
            return view('Suggestion.suggestionList',
                compact('profilesList', 'tagsListJson'),
                ['searchValue' => $inputsList, 'sortReferenceAttributeList'=> $this->sortReferenceAttributeList, 'orderList' => $this->orderList]);
        }

        $profilesList = SearchEngine::sort($userListFound, $sortReferenceAttribute, $order);

        if (($nbrProfiles = count($profilesList)) > 1)
            Session::flash('Success', $nbrProfiles.' profiles found');

        return view('Suggestion.suggestionList',
            compact('profilesList', 'tagsListJson'),
            ['searchValue' => $inputsList, 'sortReferenceAttributeList'=> $this->sortReferenceAttributeList, 'orderList' => $this->orderList]);
    }

}
