<?php

namespace App\Http\Controllers;

use App\Libraries\Test;

/*
** Controller de test pour les essaie de la lib
*/

class TestLibController extends Controller
{
    public function exemple()
    {
        dd(Test::coucou());
    }
}
