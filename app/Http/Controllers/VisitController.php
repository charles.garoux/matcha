<?php


namespace App\Http\Controllers;


use App\Libraries\Debug;
use App\Libraries\Entities\Like;
use App\Libraries\Entities\Visit;
use App\Libraries\Utils\Enums\LogicalOperator;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;

class VisitController extends Controller
{

    public function listing()
    {
        $idUser = Session::get('user.id');

        if (($visitList = Visit::listDistinctVisits($idUser)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Visit::listBy('idTarget', $idUser) === false");
            Session::flash('Error', "Unable to find visits in database");
            $visitList = [];
        }
        else if ($visitList === ReturnCase::Empty)
            $visitList = [];

        $VisitExtendList = array();
        foreach ($visitList as $visit) {
            try {
                $VisitExtend = new Visit(['dbObject' => $visit]);
                array_push($VisitExtendList, $VisitExtend);
            } catch (\Exception $e) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 3, __FUNCTION__], ' new Visit([\'dbObject\' => $visit]) => '. $e->getMessage());
                Session::flash('Error', "Error from database during the search for information on a visit");
            }
        }

        if (($likeList = Like::listByExtend(['idTarget' => $idUser], LogicalOperator::AND)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Visit::listBy('idTarget', $idUser) === false");
            Session::flash('Error', "Unable to find likes in database");
            $likeList = [];
        }
        else if ($likeList === ReturnCase::Empty)
            $likeList = [];

        $LikeExtendList = array();
        foreach ($likeList as $visit) {
            try {
                $LikeExtend = new Like(['dbObject' => $visit]);
                array_push($LikeExtendList, $LikeExtend);
            } catch (\Exception $e) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 3, __FUNCTION__], ' new Like([\'dbObject\' => $visit]) => '. $e->getMessage());
                Session::flash('Error', "Error from database during the search for information on a visit");
            }
        }

        return (view('Profile.my-visitors&Likes', compact('VisitExtendList', 'LikeExtendList')));
    }
}
