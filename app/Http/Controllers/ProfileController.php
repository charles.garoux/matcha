<?php


namespace App\Http\Controllers;


use App\Libraries\Debug;
use App\Libraries\Entities\Like;
use App\Libraries\Entities\Notification;
use App\Libraries\Entities\User;
use App\Libraries\Entities\Visit;
use App\Libraries\Utils\Enums\LogicalOperator;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;
use App\Libraries\Utils\Validate;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function show(Request $request)
    {
        $idUser = Session::get('user.id');
        $inputsList = [
            'idProfile' => ['value' => ($request->route('idProfile') ?? NULL),'type' => 'int'],
        ];
        if (($errorMessageToUser = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
        }
        $idProfile = $inputsList['idProfile']['value'];

        try {
            $Profile = new User(['id' => $idProfile], ['compatibility', 'distance']);
            if ($Profile->checkCompletion() === false)
                return (view('Utils.warning', ['message' => 'The profile is not complete']));
        } catch (\Exception $e) {
            $errorCode = $e->getCode();
            if ($errorCode != 101)
                return Debug::view(SignalType::Warning, 'DB', [__FILE__, __LINE__ - 4, __FUNCTION__], "new User(['id' => $idProfile]) => " . $e->getMessage(), $e->getMessage());
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], "new User(['id' => $idProfile]) => " . $e->getMessage(), "This profile does not exist");
        }


        if (($liked = $this->checkIfExist('Like', $idUser, $idProfile)) === ReturnCase::Error) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " \$this->checkIfExist('Like', $idUser, $idProfile) === ReturnCase::Error");
            $liked = false;
        }

        if (($blocked = $this->checkIfExist('Blocking', $idUser, $idProfile)) === ReturnCase::Error) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " \$this->checkIfExist('Blocking', $idUser, $idProfile) === ReturnCase::Error");
            $blocked = false;
        }

        if (($reported = $this->checkIfExist('Report', $idUser, $idProfile)) === ReturnCase::Error) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " \$this->checkIfExist('Report', $idUser, $idProfile) === ReturnCase::Error");
            $reported = false;
        }

        if ($idUser != $idProfile && Visit::insert(['idSender' => $idUser, 'idTarget' => $idProfile]) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Visit::insert(['idSender' => $idUser, 'idTarget' => $idProfile] === false");
        }

        if ($idUser !== $idProfile)
            Notification::push($idProfile, 'New visit', route('profile.public', ['idProfile' => $idUser]));

        return view('Profile.publicProfile', compact('Profile'), ['liked' => $liked, 'blocked' => $blocked, 'reported' => $reported]);
    }

    public function likeBtn(Request $request)
    {
        $idUser = Session::get('user.id');
        $inputsList = [
            'idProfile' => ['value' => ($request->route('idProfile') ?? NULL),'type' => 'int'],
        ];
        if (($errorMessageToUser = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
        }
        $idProfile = $inputsList['idProfile']['value'];
        if ($idUser === $idProfile)
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], '$idUser === $idProfile', 'You can\'t like yourself');

        if (($backLike = Like::getByExtend(['idTarget' => $idUser, 'idSender' => $idProfile], LogicalOperator::AND)) === false) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Like::getByExtend(['idTarget' => $idUser, 'idSender' => $idProfile], LogicalOperator::AND) === false", 'An error occurred while processing the like');
        }
        else if ($backLike == ReturnCase::Empty)
            $liked = false;
        else
            $liked = true;

        if (($action = $this->switchBtn('Like', $idUser, $idProfile)) === false) {
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " \$this->switchBtn('Like', $idUser, $idProfile) === false", 'An error occurred while processing the like');
        }

        if ($liked === false && $action == 'insert')
            Notification::push($idProfile, 'New like', route('profile.public', ['idProfile' => $idUser]));
        if ($liked && $action == 'delete')
            Notification::push($idProfile, 'A match has been lost', route('profile.public', ['idProfile' => $idUser]));
        if ($liked && $action == 'insert'){
            Notification::push($idProfile, 'New match, chat with your match', route('chat', ['idContact' => $idUser]));
            Notification::push($idUser, 'New match, chat with your match', route('chat', ['idContact' => $idProfile]));
        }

        return redirect(route('profile.public', ['idProfile' => $idProfile]));
    }

    public function blockBtn(Request $request)
    {
        $idUser = Session::get('user.id');
        $inputsList = [
            'idProfile' => ['value' => ($request->route('idProfile') ?? NULL),'type' => 'int'],
        ];
        if (($errorMessageToUser = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
        }
        $idProfile = $inputsList['idProfile']['value'];
        if ($idUser === $idProfile)
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], '$idUser === $idProfile', 'You can\'t block yourself');


        if ($this->switchBtn('Blocking', $idUser, $idProfile) === false)
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " \$this->switchBtn('Blocking', $idUser, $idProfile) === false", 'An error occurred while processing the blocking');

        return redirect(route('profile.public', ['idProfile' => $idProfile]));
    }

    public function reportBtn(Request $request)
    {
        $idUser = Session::get('user.id');
        $inputsList = [
            'idProfile' => ['value' => ($request->route('idProfile') ?? NULL),'type' => 'int'],
        ];
        if (($errorMessageToUser = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
        }
        $idProfile = $inputsList['idProfile']['value'];
        if ($idUser === $idProfile)
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], '$idUser === $idProfile', 'You can\'t report yourself');


        if ($this->switchBtn('Report', $idUser, $idProfile) === false)
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " \$this->switchBtn('Report', $idUser, $idProfile) === false", 'An error occurred while processing the report');

        return redirect(route('profile.public', ['idProfile' => $idProfile]));
    }

    /**
     * Switch the status if the element corresponding
     *
     * @param string $className : The classname of the Entity
     *      Class list : Like|Report|Blocking
     * @param $idSender
     * @param $idProfile
     * @return string|bool
     *      "insert' : insert
     *      "delete" : delete
     *      false : error
     */
    private function switchBtn(string $className, $idSender, $idProfile)
    {
        $class = "\App\Libraries\Entities\\$className";
        if (($data = $class::getByExtend(['idSender' => $idSender, 'idTarget' => $idProfile], LogicalOperator::AND)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $class::getByExtend(['idSender' => $idSender, 'idTarget' => $idProfile], LogicalOperator::AND) === false");
            return (false);
        }
        else if ($data == ReturnCase::Empty) {
            if ($class::insert(['idSender' => $idSender, 'idTarget' => $idProfile]) === false) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $class::insert(['idSender' => $idSender, 'idTarget' => $idProfile]) === false");
                return (false);
            }
            return ('insert');
        }
        else {
            if ($class::deleteBy('id', $data->id) === false) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $class::deleteBy('id', $data->id) === false");
                return (false);
            }
            return ('delete');
        }
    }

    /**
     * @param string $className
     *      Class list : Like|Report|Blocking
     * @param $idSender
     * @param $idProfile
     * @return bool|string
     */
    private function checkIfExist(string $className, $idSender, $idProfile)
    {
        $class = "\App\Libraries\Entities\\$className";
        if (($data = $class::getByExtend(['idTarget' => $idProfile, 'idSender' => $idSender], LogicalOperator:: AND)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $class::getByExtend(['idTarget' => $idProfile, 'idSender' => $idSender], LogicalOperator:: AND) === false");
            return (ReturnCase::Error);
        }
        else if ($data === ReturnCase::Empty)
            return (false);
        return (true);
    }
}
