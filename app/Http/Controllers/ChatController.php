<?php


namespace App\Http\Controllers;


use App\Libraries\Debug;
use App\Libraries\Entities\Like;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;

class ChatController extends Controller
{
    public function init($idContact)
    {
        $idUser = Session::get('user.id');

        if (($match = Like::getMatch($idUser, $idContact)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Like::getMatch($idUser, $idContact) === false");
            $message = "Database error during match verification";
            return view('Utils.error', compact('message'));
        }
        else if ($match === ReturnCase::Empty) {
            $message = "You can only talk to the user you have match";
            return view('Utils.warning', compact('message'));
        }

        return view('Chat.chatWindow', ['idContact' => $idContact, 'idUser' => Session::get('user.id')]);
    }
}
