<?php


namespace App\Http\Controllers;


use App\Libraries\Debug;
use App\Libraries\Entities\Like;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;
use \Exception;

class MatchController extends Controller
{
    public function listing()
    {
        $idUser = Session::get('user.id');
        if (($matchList = Like::listMatchsByidUser($idUser)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Visit::listBy('idTarget', $idUser) === false");
            Session::flash('Error', "Unable to find database visits");
            $matchList = [];
        }
        else if ($matchList === ReturnCase::Empty) {
            Session::flash('Warning', 'No visits found');
            $matchList = [];
        }

        $MatchExtendList = array();
        foreach ($matchList as $match) {
            try {
                $MatchExtend = new class{
                    public $data;
                    public $Profile;
                };
                $MatchExtend->data = $match;
                $MatchExtend->Profile = new User(['id' => $match->idTarget]);
                array_push($MatchExtendList, $MatchExtend);
            } catch (Exception $e) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 3, __FUNCTION__], ' new Visit([\'dbObject\' => $visit]) => '. $e->getMessage());
                Session::flash('Error', "Error from database during the search for information on a visit");
            }
        }
        return (view('Profile.my-match', compact('MatchExtendList')));
    }
}
