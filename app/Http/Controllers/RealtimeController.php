<?php


namespace App\Http\Controllers;


use App\Libraries\Debug;
use App\Libraries\Entities\ConnectionTicket;
use App\Libraries\Entities\message;
use App\Libraries\Entities\Notification;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;
use App\Libraries\Utils\Validate;
use \Exception;
use Illuminate\Http\Request;

class RealtimeController extends Controller
{
    /**
     * WARNING :
     * This class is used only with AJAX requests, all methods return a JSON or a string (true or false)
     */


    /**
     * Get the connection status of the targeted user
     *
     * @param Request $request
     * @return false|string
     */
    public function isAlive(Request $request)
    {
        $response = array('status' => 'KO', 'reason' => '', 'message' => '', 'alive' => false, 'lastAlive' => 0);

        $inputsList = [
            'idUser' => ['value' => ($request->route('idUser') ?? NULL),'type' => 'int'],
        ];
        if (Validate::check($inputsList) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            $response['reason'] = '[Fail] Input validation';
            $response['message'] = 'Error in inputs';
            return (json_encode($response));
        }
        $idUser = $inputsList['idUser']['value'];

        if (($connection = ConnectionTicket::getBy('idUser', $idUser)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "ConnectionTicket::getBy('idUser', $idUser) === false");
            $response['reason'] = '[Fail] Find notification';
            $response['message'] = "Can't find the notification";
            return (json_encode($response));
        } else if ($connection === ReturnCase::Empty) {
            $response['message'] = "No tickets found";
            return (json_encode($response));
        }

        if ((time() - $connection->lastAlive) > env("KEEPALIVE_VALIDITY")) {
            $response['lastAlive'] = $connection->lastAlive;
            $response['reason'] = '[Expiration] Ticket is expired';
            $response['message'] = 'User is not logged in';
            return (json_encode($response));
        }

        $response['alive'] = true;
        $response['lastAlive'] = $connection->lastAlive;
        $response['status'] = 'OK';
        return (json_encode($response));
    }

    /**
     * Informs that the user is still logged in
     * and return the keepAlive status and the lasts notifications
     * already not views.
     *
     * @return string(json)|false
     */
    public function keepAlive()
    {
        $idUser = Session::get('user')['id'];
        $response = array('status' => 'KO', 'reason' => '', 'message' => '', 'keepedAlive' => false, 'notifications' => null);

        try {
            $connection = new ConnectionTicket(['idUser' => $idUser], false);
            if ($connection->lastAlive + env("AJAX_FREQUENCY") < time()) {
                $connection->lastAlive = time();
                $connection->save();
            }
        } catch (Exception $e) {
            if ($e->getCode() == 101) {
                if (ConnectionTicket::insert(['idUser' => $idUser, 'lastAlive' => time()]) === false) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "ConnectionTicket::insert(['idUser' => $idUser, 'lastAlive' => time()]) === false");
                    $response['reason'] = "[FAIL] Create connection ticket";
                    $response['message'] = "Can't create ticket to report connection";
                }
            }
            else if ($e->getCode() == 102) {
                $response['reason'] = '[Fail] Update connection ticket';
                $response['message'] = "Can't change the notification status \"to seen\"";
            }
            else {
                $response['reason'] = 'Unknown reason';
                $response['message'] = "Can't update connection status";
            }
        }
        $response['keepedAlive'] = true;

        if (($response['notifications'] = Notification::listNotViews($idUser)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "Notification::listNotViews($idUser) === false");
            $response['reason'] .= "[FAIL] List notifications not views";
            $response['message'] .= "Can't find notifications";
            return (json_encode($response));
        }

        $response['status'] = 'OK';
        return (json_encode($response));
    }

    /**
     * Get the notification
     *
     * @param Request $request
     * @return string(json)|false : JSON contains the notification
     */
    public function getNotification(Request $request)
    {
        $response = array('status' => 'KO', 'reason' => '', 'message' => '', 'notification' => null);

        $inputsList = [
            'idNotification' => ['value' => ($request->route('idNotification') ?? NULL),'type' => 'int'],
        ];
        if (Validate::check($inputsList) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            $response['reason'] = '[Fail] Input validation';
            $response['message'] = 'Error in inputs';
            return (json_encode($response));
        }
        $idNotification = $inputsList['idNotification']['value'];

        try {
            $notification = new Notification(['id' => $idNotification], false);
            $notification->isSeen = true;
            try {
                $notification->save();
            } catch (Exception $e) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], " $notification->save() => " . $e->getmessage());
                $response['reason'] = '[Fail] Update notification status';
                $response['message'] = "Can't change the notification status \"to seen\"";
                return (json_encode($response));
            }
        } catch (Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], "new Notification(['id' => $idNotification]) => " . $e->getmessage());
            $response['reason'] = '[Fail] Find notification';
            $response['message'] = "Can't find the notification";
            return (json_encode($response));
        }

        $response['status'] = 'OK';
        $response['notification'] = $notification;
        return (json_encode($response));
    }

    /**
     * Get all messages between the user and their contact
     *
     * @param Request $request
     * @return false|string
     */
    public function getChat(Request $request)
    {
        $idUser = Session::get('user.id');
        $response = array('status' => 'KO', 'reason' => '', 'message' => '', 'messages' => null);

        $inputsList = [
            'idContact' => ['value' => ($request->route('idContact') ?? NULL),'type' => 'int'],
        ];
        if (Validate::check($inputsList) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            $response['reason'] = '[Fail] Input validation';
            $response['message'] = 'Error in inputs';
            return (json_encode($response));
        }
        $idContact = $inputsList['idContact']['value'];

        if (($messages = message::listConversationmessages($idUser, $idContact)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], "message::listConversationmessages($idUser, $idContact) === false");
            $response['reason'] = '[Fail] Find messages';
            $response['message'] = "Can't find messages";
            return (json_encode($response));
        }
        else if ($messages == ReturnCase::Empty){
            $response['reason'] = 'Empty conversation';
            $response['message'] = "No message in this conversation";
            return (json_encode($response));
        }

        $messagesExtend = array();
        foreach ($messages as $key => $message) {
            try {
                array_push($messagesExtend, new message(['id' => $message->id]));
            } catch (Exception $e) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "new message(['id' => $message->id]". $e->getmessage());
                $response['reason'] = '[Fail] Instantiate messages';
                $response['message'] = "Can't prepare messages";
                return ("false");
            }
        }

        $response['status'] = 'OK';
        $response['messages'] = $messagesExtend;
        return (json_encode($response));
    }

    /**
     * Save the message in the database
     *
     * @param Request $request
     *      - int idContact
     *      - string text
     * @return string
     */
    public function sendMessage(Request $request)
    {
        $idUser = Session::get('user.id');
        $response = array('status' => 'KO', 'reason' => '', 'message' => '');

        $inputsList = [
            'idContact' => ['value' => ($request->input('idContact') ?? NULL),'type' => 'int'],
            'text' => ['value' => ($request->input('text') ?? NULL),'type' => 'str'],
        ];
        if (Validate::check($inputsList) !== true) {
            Debug::print(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false');
            $response['reason'] = '[Fail] Input validation';
            $response['message'] = 'Error in inputs';
            return (json_encode($response));
        }
        $idTarget = $inputsList['idContact']['value'];
        $text = $inputsList['text']['value'];

        if (message::insert(['idSender' => $idUser, 'idTarget' => $idTarget, 'text' => $text]) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], "message::insert(['idSender' => $idUser, 'idTarget' => $idTarget, 'text' => $text]) === false");
            $response['reason'] = "[FAIL] Save message";
            $response['message'] = "The message could not be sent";
            return (json_encode($response));
        }

        $chatRoute = route('chat', ['idContact' => $idUser]);
        $notificationMessage = 'You have a new message';
        try {
            $notification = new Notification(['idUser' => $idTarget, 'message' => $notificationMessage,'link' => $chatRoute], false);
            $notification->timestamp = date("Y-m-d H-i-s");
            $notification->isSeen = false;
            $notification->save();
        } catch (Exception $e) {
            /**
             * 100 isn't a fatal error for the application
             * 101 is not an error
             */
            if ($e->getCode() == 100) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 9, __FUNCTION__], "new Notification(['idUser' => $idTarget])");
            }
            else if ($e->getCode() == 102) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 10, __FUNCTION__], " \$notification->save()");
            }
            else if ($e->getCode() == 101) {
                if (Notification::push($idTarget, $notificationMessage, $chatRoute) === false) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], " Notification::push($idTarget, $notificationMessage, $chatRoute) === false");
                    $response['reason'] = "[FAIL] Push notification";
                    $response['message'] = "Your contact could not be notified of your message";
                    return (json_encode($response));
                }
            }
        }

        $response['status'] = 'OK';
        return (json_encode($response));
    }
}
