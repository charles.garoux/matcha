<?php

namespace App\Http\Controllers;


use App\Libraries\Utils\Auth;

class DisconnectController extends Controller
{
    public function signOut()
    {
        Auth::signOut();
        return redirect(route('login.form'));
    }
}
