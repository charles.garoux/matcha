<?php

namespace App\Http\Controllers;

use App\Libraries\Debug;
use App\Libraries\Entities\ResetTicket;
use App\Libraries\Entities\Ticket;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Auth;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;
use App\Libraries\Utils\Validate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgetPasswordController extends Controller
{

    /**
     * Simple redirection sur la view 'ForgetPassword.forgetPassword'
     *
     * @return \Illuminate\View\View
     */
    public function form()
    {
        return view('ForgetPassword.forgetPassword');
    }

    public function modifyPassword($token)
    {
        $inputsList = [
            'password' => ['value' => ($_POST['new_password'] ?? NULL), 'type' => 'str'],
            'confirm_password' => ['value' => ($_POST['confirm_password'] ?? NULL), 'type' => 'str']
        ];

        if (($errorMessageToUser = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
        }

        if(strcmp($inputsList['password']['value'], $inputsList['confirm_password']['value']) !== 0) {
            Session::flash('Warning', "Your password confirmation is bad");
            return view('ForgetPassword.modifyPassword');
        }

        try {
            $Ticket = new ResetTicket(['key' => $token]);
            $Ticket->User->password = Hash::make($inputsList['password']['value']);
            $Ticket->User->save();
        }
        catch (\Exception $e) {
            if ($e->getCode() == 100) {
                $errorMessageToUser = "Error from the DB";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 7, __FUNCTION__], 'new ResetTicket() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 101) {
                $errorMessageToUser = "Searched entity doesn't exist";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 11, __FUNCTION__], 'new ResetTicket() === false', $errorMessageToUser);
            }
            if ($e->getCode() == 102) {
                $errorMessageToUser = "Error from the DB, can't update";
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 13, __FUNCTION__], 'save(); === false', $errorMessageToUser);
            }
        }
        ResetTicket::deleteBy('key', $token);
        if (Auth::isIt()) {
            return redirect(route('account.edit.information'));
        }
        return redirect(route('login.form'));
    }

    /**
     * @param $token
     *
     * Check si l'utilisateur a l'autorisation de modifier son mot de passe.
     * Verifie si le token est valide.
     *
     * @return \Illuminate\View\View
     */
    public function tokenValidation($token)
    {
        if (($DB_Token = ResetTicket::getBy('key', $token)) !== false) {
            if ($DB_Token !== "Empty") {
                if (strcmp($DB_Token->key, $token) === 0) {
                    return view('ForgetPassword.modifyPassword', ['token' => $DB_Token->key]);
                }
                else {
                    Session::flash('Warning', "Token not good or expired");
                    $message = "Please check your email to modify the password.";
                    return view('Utils.success', compact('message'));
                }
            }
            else {
                Session::flash('Warning', "Token not good or expired");
                $message = "Please check your email to modify the password.";
                return view('Utils.success', compact('message'));
            }
        }
        else {
            $errorMessageToUser = "There is an error with the database";
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 18, __FUNCTION__], 'RegisterTicket::getBy() === false', $errorMessageToUser);
        }
    }


    public function resetPassword()
    {
        $inputsList = [
            'login' => ['value' => ($_POST['login'] ?? NULL), 'type' => 'str']
        ];
        if (($errorMessageToUser = Validate::check($inputsList)) !== true) {
            return Debug::view(SignalType::Warning, 'VALIDATION', [__FILE__, __LINE__ - 1, __FUNCTION__], 'Validate::check() === false', $errorMessageToUser);
        }
        if (($user = User::getBy('login', $inputsList['login']['value'])) !== false) {
            if ($user === "Empty") {
                Session::flash('Warning', "Unknown login");
                return view('ForgetPassword.forgetPassword');
            }

            $user = [
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'login' => $user->login,
                'password' => $user->password,
                'email' => $user->email
            ];
            $token = Ticket::generator();
            $route = route('forgetPassword.validation.processing', compact('token'));
            $viewData = array('user' => $user, 'appAddress' => $route, 'token' => $token);

            if (ResetTicket::insert(['key' => $token, 'idUser' => $user['id']]) !== false)
            {
                Mail::send('Emails.ModifPassword', $viewData, function($message) use($user)
                {
                    $message->to($user['email'])
                        ->subject('Password reset');
                });
                $message = "Please check your email to modify the password.";
                return view('Utils.success', compact('message'));
            }
            else {
                $errorMessageToUser = "There is an error with the database";
                ResetTicket::deleteBy('idUser', $user['id']);
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 11, __FUNCTION__], 'RegisterTicket::insert() === false', $errorMessageToUser);
            }
        }
        else {
            $errorMessageToUser = "There is an error with the database";
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 36, __FUNCTION__], 'User::getBy() === false', $errorMessageToUser);
        }
    }
}
