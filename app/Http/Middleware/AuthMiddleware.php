<?php

namespace App\Http\Middleware;

use App\Libraries\Utils\Auth;
use App\Libraries\Utils\Session;
use Closure;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::isIt() === false) {
            Session::flash('Error', 'To access this part of the application you must be logged in');
            return redirect(route('login.form'));
        }
        return $next($request);
    }
}
