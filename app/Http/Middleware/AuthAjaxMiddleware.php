<?php

namespace App\Http\Middleware;

use App\Libraries\Utils\Auth;
use App\Libraries\Utils\Session;
use Closure;

class AuthAjaxMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::isIt() === false) {
            http_response_code(2001);
            return "false";
        }
        return $next($request);
    }
}
