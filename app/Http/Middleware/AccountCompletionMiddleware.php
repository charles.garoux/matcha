<?php

namespace App\Http\Middleware;

use App\Libraries\Debug;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;
use Closure;

class AccountCompletionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $idUser = Session::get('user.id');
            $User = new User(['id' => $idUser]);
        } catch (\Exception $e) {
            $errorCode = $e->getCode();
            if ($errorCode != 101)
                return Debug::view(SignalType::Warning, 'DB', [__FILE__, __LINE__ - 4, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
            return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
        }

        if ($User->checkCompletion() === false)
        {
            Session::flash('Error', 'To access this part of the application your profile must be complete');
            return redirect(route('account.edit.information'));
        }

        return $next($request);
    }
}
