<?php


namespace App\Libraries\Utils;


use App\Libraries\Debug;
use App\Libraries\Entities\RegisterTicket;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use \Exception;
use Illuminate\Support\Facades\Hash;

class Auth extends Session
{
    /**
     * Check is the user is authenticated
     * @return bool
     */
    static public function isIt()
    {
        $is_auth = isset($_SESSION['user']);

        /**
         * Check if the user still exist
         */
        if ($is_auth && User::getBy('login', $_SESSION['user']['login']) === ReturnCase::Empty) {
            unset($_SESSION['user']);
            return (false);
        }

        return ($is_auth);
    }


    /**
     * Check if the username/password pair allows the user to connect
     * and if the account is validated
     *
     * @param $login
     * @param $password
     * @return bool
     * @throws Exception
     */
    static public function signIn($login, $password)
    {
        if (($User = User::getBy('login', $login)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "User::getBy('login', $login) === false");
            throw new Exception('Error from the DB', 200);
        }
        else if ($User === ReturnCase::Empty) {
            throw new Exception('Unknown login', 201);
        }

        try {
            if (self::isValidate($User->id) === false)
                throw new Exception('The account has not been validated', 203);
        } catch (Exception $e) {
            if ($e->getCode() == 200) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "self::isValidate($User->id) === false");
                throw new Exception($e->getMessage(), $e->getCode());
            }
            throw new Exception($e->getMessage(), $e->getCode());
        }

        if (Hash::check($password, $User->password) === false) {
            throw new Exception('The password is incorrect', 202);
        }

        self::set('user', ['id' => $User->id, 'login' => $User->login]);

        return (true);
    }

    /**
     * Delete user authentication data
     */
    static public function signOut()
    {
        self::delete('user');
    }

    /**
     * Check if the user has been validated
     *
     * @param $idUser
     * @return bool
     * @throws Exception
     */
    static public function isValidate($idUser)
    {
        if (($registerTicket = RegisterTicket::getBy('idUser', $idUser)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "RegisterTicket::getBy('idUser', $idUser)) === false");
            throw new Exception('Error from the DB', 200);
        }
        else if ($registerTicket !== ReturnCase::Empty) {
            return (false);
        }

        return (true);
    }

}
