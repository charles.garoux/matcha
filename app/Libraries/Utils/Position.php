<?php

namespace App\Libraries\Utils;

use App\Libraries\DB\Tables\DB_Table;

class Position
{

    /**
     * @param $adress
     *
     * Creation de la pos latitude,longitude
     * grace au formulaire.
     *
     * @return array()
     */
    static public function getByAdress($adress)
    {
        $c=curl_init("https://nominatim.openstreetmap.org/search?q=".urlencode($adress)."&format=xml&polygon=0&addressdetails=0");
        curl_setopt($c, CURLOPT_USERAGENT,'http://pgc.petschge.de');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true );
        $r=curl_exec($c);

        $p = xml_parser_create();
        xml_parse_into_struct($p, $r, $vals, $index);
        xml_parser_free($p);

        if (isset($vals['1']['attributes']['LAT']) === false || isset($vals['1']['attributes']['LON']) === false)
            return (false);
        $lat = $vals['1']['attributes']['LAT'];
        $lon = $vals['1']['attributes']['LON'];
        $pos = array("lat" => $lat, "lon" => $lon);
        return ($pos);
    }


    /**
     * Creation de la pos latitude,longitude grace a l'IP de l'utilisateur.
     * @return array()
     */
    static public function getByIP()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $details = json_decode(file_get_contents("http://ipinfo.io/". $ip ."/json"));
        if (isset($details->loc))
        {
            $lat = substr($details->loc, 0, strpos($details->loc, ","));
            $lon = str_replace(",", "", strrchr($details->loc, ","));
            $pos = array("lat" => $lat, "lon" => $lon);
            return ($pos);
        }
    }

}
