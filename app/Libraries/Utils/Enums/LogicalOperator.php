<?php


namespace App\Libraries\Utils\Enums;

Use App\Libraries\Utils\Enums\BasicEnum;

class LogicalOperator extends BasicEnum
{
    const AND = 'AND';
    const OR = 'OR';
}
