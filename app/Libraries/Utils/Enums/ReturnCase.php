<?php

namespace App\Libraries\Utils\Enums;


abstract class ReturnCase extends BasicEnum
{
	const Error = 'Error';
    const Empty = 'Empty';
    const Deny = 'Deny';
}
