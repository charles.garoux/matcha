<?php


namespace App\Libraries\Utils\Enums;


class FlashType extends BasicEnum
{
    const Success = 'Success';
    const Info = 'Info';
    const Warning = 'Warning';
    const Error = 'Error';
}
