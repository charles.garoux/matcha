<?php


namespace App\Libraries\Utils\Enums;


class SignalType extends BasicEnum
{
    const Error = 'Error';
    const Warning = 'Warning';
    const Success = 'Success';
}
