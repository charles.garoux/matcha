<?php

namespace App\Libraries\Utils;

use App\Libraries\Utils\Key;

class Logger
{
	static private $request_id = null;
	static private $log_path = null;

	static private function setup()
	{
		self::$request_id = Key::generate(env('REQUEST_ID_LENGTH')) .' '. date("d-m-Y_H:i:s") .' '. $_SERVER['REQUEST_URI'];
		self::$log_path = env('LOG_FILE_PATH');
	}

	static public function write($message_log)
	{
		if (self::$request_id == null)
			self::setup();

		if (file_exists(self::$log_path) === False) {
			echo "[ERREUR] ".__FILE__." :Le fichier de journal \"". self::$log_path ."\" n'existe pas. Il doit etre creer pour faire fonctionner le mode \"SENDING_MAIL\" en \"DEBUG\" ou le mode \"DEBUG\" en \"LOG\" " . PHP_EOL;
			exit();
		}
		if (is_writable(self::$log_path) === false) {
			echo "[ERREUR] ".__FILE__." :Le fichier de journal \"". self::$log_path ."\" ne peu pas etre ecrit" . PHP_EOL;
			exit();
		}

		if (file_put_contents(self::$log_path, '['. self::$request_id .'] '. $message_log, FILE_APPEND) === false) {
			echo '[ERREUR] '.__FILE__.' '. __FUNCTION__ .'()'. (__LINE__ - 1) .': Impossible d\'ecrire dans le ficher de journal '. self::$log_path . PHP_EOL;
			exit();
		}
	}
}

?>
