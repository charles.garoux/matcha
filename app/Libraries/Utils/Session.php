<?php


namespace App\Libraries\Utils;


class Session
{
    /**
     * Start and prepare sessions
     */
    static public function start()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();

        self::purgeFlash();
    }

    /**
     * Get all elements in session
     *
     * @return mixed
     */
    static public function getAll()
    {
        return ($_SESSION);
    }

    /**
     * Check if an attribut exist in session
     *
     * @param $attribut
     * @return bool
     */
    static public function has($attribut)
    {
        if (isset($_SESSION[$attribut]) === false) {
            if (strpos($attribut, '.')) {
                $pathToTargetData = explode('.', $attribut);
                $checkArray = $_SESSION;
                foreach ($pathToTargetData as $key => $value) {
                    if (isset($checkArray[$value])) {
                        $checkArray = $checkArray[$value];
                    }
                    else
                        return (false);
                }
            }
            else
                return (false);
        }

        return (true);
    }

    /**
     * Get element in session
     *
     * @param $attribut
     * Example, retrieving the id of the user that is contained in the "id" attribute of the "user" array :
        'user.id'
     * @return mixed|bool
     */
    static public function get($attribut)
    {
        if (isset($_SESSION[$attribut]) === false) {
            if (strpos($attribut, '.')) {
                $pathToTargetData = explode('.', $attribut);
                $data = $_SESSION;
                $checkArray = $_SESSION;
                foreach ($pathToTargetData as $key => $value) {
                    if (isset($checkArray[$value])) {
                        $data = $data[$value];
                        $checkArray = $checkArray[$value];
                    }
                    else
                        return (false);
                }
            }
            else
                return (false);
        }

        return ($data ?? $_SESSION[$attribut]);
    }

    /**
     * Set element in session
     *
     * @param $attribut
     * @param $value
     */
    static public function set($attribut, $value)
    {
        $_SESSION[$attribut] = $value;
    }

    /**
     * Delete element in session
     *
     * @param $attribut
     */
    static public function delete($attribut)
    {
        unset($_SESSION[$attribut]);
    }

    /**
     * Delete all data in session
     * or a sub array of the session array
     *
     * @param array|null $dataList
     */
    static public function purge(array &$dataList = null)
    {
        if ($dataList == null)
            session_unset();
        else
            foreach ($dataList as $attribut => $value) {
                unset($dataList[$attribut]);
            }
    }

    /* ========================== FLASH ==========================  */
    /**
     * Flash :
     *  A flash is a temporary message (existing only for the next page displayed),
     *  it is used regularly to send a message to the user
     */

    /**
     * Record a flash in session
     *
     * @param $flashType : Success|Info|Warning|Error
     * @param string $message
     */
    static public function flash($flashType, string $message)
    {
        $_SESSION['flash'][$flashType] = $message;
    }

    /**
     * Get data store in flash by flashType
     *
     * @param $flashType
     * @return bool
     */
    static public function getFlash($flashType)
    {
        if (isset($_SESSION['flash'][$flashType]) === false)
            return (false);
        return ($_SESSION['flash'][$flashType]);
    }

    /**
     * Delete all flash
     */
    static public function purgeFlash()
    {
        self::delete('flash');
    }
}
