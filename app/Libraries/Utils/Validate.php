<?php

namespace App\Libraries\Utils;

use App\Libraries\Debug;
use App\Libraries\Utils\Enums\SignalType;
use Exception;

class Validate
{
    static $errors = true;

    /* ============= VERIFICATION DES TYPE =============== */

    static private function str($val)
    {
        if (!is_string($val)) {
            self::throwError('Invalid string', 902);
        }
        $val = trim(htmlspecialchars($val));
        return $val;
    }

    static private function int($val)
    {
        $val = filter_var($val, FILTER_VALIDATE_INT);
        if ($val === false) {
            self::throwError('Invalid integer', 901);
        }
        return $val;
    }

    static private function bool($val)
    {
        $val = filter_var($val, FILTER_VALIDATE_BOOLEAN);
        return $val;
    }

    static private function email($val)
    {
        $val = filter_var($val, FILTER_VALIDATE_EMAIL);
        if ($val === false) {
            self::throwError('Invalid email', 903);
        }
        return $val;
    }

    static private function enum($val, array $enumList)
    {
        if (in_array($val, $enumList) === false) {
            self::throwError('Invalid enum', 905);
        }
        return ($val);
    }


    /**
     * @param array $array
     * @param $keyType
     *      If the type is forceInt then all the keys of the array will be replaced by integers
     * @param $valueType
     * @return array
     * @throws Exception
     */
    static private function array(array $array, $keyType, $valueType)
    {
        if ($keyType == 'forceInt')
            $array = array_values($array);
        $newArray = array();
        foreach ($array as $key => $value) {
            if ($keyType != 'forceInt') {
                try {
                    $newKey = self::$keyType($key);
                } catch (Exception $e) {
                    self::throwError("Invalide array key [" . $key . "] => " . $e->getMessage(), 906);
                }
            }
            else
                $newKey = $key;
            try {
                $newArray[$newKey] = self::$valueType($value);
            } catch (Exception $e) {
                self::throwError("Invalide array[$key] => ". $e->getMessage() , 907);
            }
        }
        return ($newArray);
    }

    static private function login($val)
    {
        if (strlen($val) > 32)
            self::throwError('Login too long (32 characters maximum)', 904);
        else if (strlen($val) < 3)
            self::throwError('Login too short (3 characters minimum)', 904);
        return (self::str($val));
    }

    static function base64($val) {
        $check = base64_encode(base64_decode($val));
        if ($val != $check || $check === false) {
            self::throwError('base64 invalide', 908);
        }
        return ($check);
    }

    /**
     * (The value 1366370 was empirically found after many tries on another project)
     * (The value 7803743 was empirically found during this project)
     * Empirical value :
     * - old :    1366370
     * - new :    7803743 (5 852 790 bytes (5,9 MB on disk))
     *
     * @param $val
     * @return bool|string
     * @throws Exception
     */
    static function imageBase64($val) {
        if (strlen($val) >= 7803743) {
            self::throwError('Image too heavy', 909);
        }
        if (($imageSliced = explode(',', $val, 2)) === false) {
            self::throwErrors('Can\'t split image to analyse', 910);
        }
        $base64String = $imageSliced[1];
        return ($imageSliced[0] .','. self::base64($base64String));
    }

    static private function float($val)
    {
        $val = filter_var($val, FILTER_VALIDATE_FLOAT);
        if ($val === false) {
            self::throwError('Invalid integer', 911);
        }
        return $val;
    }

    static private function throwError($error = 'Error In Processing', $errorCode = 0)
    {
        if (self::$errors === true) {
            throw new Exception($error, $errorCode);
        }
    }

    /* ============= NETTOYAGE DE L'ENTREE =============== */

    static private function simple_sanitize($value)
    {
        return (trim(htmlspecialchars($value)));
    }

    static private function simple_sanitize_array(array $tab)
    {
        foreach ($tab as $key => $value) {
            $tab[self::simple_sanitize($key)] = self::simple_sanitize($value);
        }
        return ($tab);
    }

    /*============ LANCEMENT DE VERIFICATION =============*/

    static private function check_completion($inputsList)
    {
        foreach ($inputsList as $key => $value) {
            if ($inputsList[$key]['value'] === NULL
                || (($inputsList[$key]['type'] != 'int' && $inputsList[$key]['type'] != 'array') && empty($inputsList[$key]['value']))) {
                self::throwError('Information (' . $key . ') manquante', 900);
            }
        }
    }

    /**
     * Launch checks on inputs list
     *
     * @param array $inputsList
     *   format :
      [
          'inputName' => ['value' => $_POST['inputName'], 'type' => 'typeName'],
          'inputName2' => ['value' => $_GET['inputName2'], 'type' => 'typeName2']
      ]
     *   exemple :
      $inputsList = [
          'login' => ['value' => ($_POST['login'] ?? NULL), 'type' => 'login'],
          'password' => ['value' => ($_POST['password'] ?? NULL), 'type' => 'str'],
          'email' => ['value' => ($_POST['email'] ?? NULL), 'type' => 'email'],
          'receiveEmail' => ['value' => ($_POST['receiveEmail'] ?? NULL), 'type' => 'bool'],
          'tagslist' => ['value' => ($_POST['tagslist'] ?? NULL, 'type' => 'array', 'arrayKeyType' => 'int', 'arrayValueType' => 'str'],
          'unknowKeylist' => ['value' => ($_POST['unknowKeylist'] ?? NULL, 'type' => 'array', 'arrayKeyType' => 'forceInt', 'arrayValueType' => 'str'],
          'tag' => ['value' => ($_POST['tag'] ?? NULL), 'type' => 'enum', 'enumList' => ['tagA', 'tagB']]
    ];
     * Tips :
     *      - to make this input mandatory use : 'value' => ($value ?? NULL)
     *
     * @return bool|string : Si different de true, alors c'est un message d'erreur
     */
    static public function check(&$inputsList)
    {
        try {
            self::check_completion($inputsList);
        } catch (Exception $exception) {
            Debug::print(SignalType::Warning, 'INPUT VALIDATION', [__FILE__, __LINE__ - 2, __FUNCTION__], $exception->getMessage());
            return ('The request is incomplete');
        }
        foreach ($inputsList as $key => $check) {
            $type = $check['type'];
            if ($type == 'array') {
                if ($check['value'] === NULL)
                    return ('Invalide array => array does not exist');
                else if (count($check['value']) > 0)
                    $check['value'] = self::simple_sanitize_array($check['value']);
            }
            else
                $check['value'] = self::simple_sanitize($check['value']);
            try {
                if ($type == 'enum')
                    $inputsList[$key]['value'] = self::$type($check['value'], $check['enumList']);
                else if ($type == 'array'){
                    $inputsList[$key]['value'] = self::$type($check['value'], $check['arrayKeyType'], $check['arrayValueType']);
                }
                else
                    $inputsList[$key]['value'] = self::$type($check['value']);
            } catch (Exception $exception) {
                Debug::print(SignalType::Warning, 'INPUT VALIDATION', [__FILE__, __LINE__ - 2, __FUNCTION__],
                    'la clé "' . $key . '" de valeur "' . ($type == 'array'? json_encode($check['value']) : $check['value']) . '" => ' . $exception->getMessage());
                return ($exception->getMessage());
            }
        }
        return (true);
    }

}
