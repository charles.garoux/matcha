<?php


namespace App\Libraries\Utils;


use App\Libraries\Debug;
use App\Libraries\Entities\Tag;
use App\Libraries\Entities\User;
use App\Libraries\Utils\Enums\LogicalOperator;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use Exception;

class SearchEngine
{
    /**
     * Count common points (tags) between the two users
     *
     * @param User $UserA
     * @param User $UserB
     * @return int
     */
    static public function countCommonPoint(User $UserA, User $UserB)
    {
        $commonPoint = 0;

        foreach ($UserA->tags as $tagName => $tagData)
        {
            if (isset($UserB->tags[$tagName]))
                $commonPoint++;
        }

        return ($commonPoint);
    }

    /* ====================== POSITION ====================== */


    /**
     * calculate the geographical position between 2 points
     *
     * @param $geoCoordinatesA
     * @param $geoCoordinatesB
     * @return int
     */
    static public function distanceBetween(array $geoCoordinatesA, array $geoCoordinatesB)
    {
        $distance = self::vincentyGreatCircleDistance($geoCoordinatesA['lat'], $geoCoordinatesA['lon'],
                                                        $geoCoordinatesB['lat'], $geoCoordinatesB['lon']);
        return (intval($distance));
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param int $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    static private function vincentyGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }

    /* ====================== ATTRACTION ====================== */

    /**
     * Check if the two users can attract each other
     *
     * @param User $UserA
     * @param User $UserB
     * @return bool
     */
    static public function checkAttraction(User $UserA, User $UserB)
    {
        foreach ($UserA->attractions as $AttractGenderA) {
            if ($AttractGenderA->id == $UserB->Gender->id){
                foreach ($UserB->attractions as $AttractGenderB) {
                    if ($AttractGenderB->id == $UserA->Gender->id){
                        return (true);
                    }
                }
            }
        }
        return (false);
    }

    /* ====================== COMPATIBILITY ====================== */
    /**
     * Compatibility scale :
     *  - one km away : -1 point
     *  - one common point : 15 points
     *  - one score point : 0.1 point
     */

    /**
     * @param $distance
     * @return int
     */
    static private function getCompatibilityFromDistance($distance)
    {
        return (intval(($distance / 1000 / 5) * -5));
    }

    /**
     * @param $commonPointNbr
     * @return int
     */
    static private function getCompatibilityFromCommonPoint($commonPointNbr)
    {
        return ($commonPointNbr * 15);
    }

    /**
     * @param $score
     * @return int
     */
    static private function getCompatibilityFromScore($score)
    {
        return (intval(round($score * 0.1)));
    }


    /**
     * Calculate the compatibility between two users
     *
     * @param User $UserA
     * @param User $UserB
     * @return int
     */
    static public function calcCompatibility(User $UserA, User $UserB)
    {
        $distance = self::distanceBetween($UserA->geoCoordinates, $UserB->geoCoordinates);
        $compatibility = self::getCompatibilityFromDistance($distance);

        if (isset($UserB->commonPoints) == false)
            $UserB->commonPoints = self::countCommonPoint($UserA, $UserB);
        $compatibility += self::getCompatibilityFromCommonPoint($UserB->commonPoints);

        $compatibility += self::getCompatibilityFromScore($UserB->score);

        return ($compatibility);
    }

    /* ====================== SEARCH USERS ====================== */

    static private function prepareTaggedInClause($tagBinding, $logicalOperator) {

        $whereClause = null;
        foreach ($tagBinding as $bind => $idTag) {
            $whereClause .= " AND id IN (SELECT idUser FROM Tagged WHERE idTag = :$bind)";
        }

        return ($whereClause);
    }

    /**
     * @param User $UserLookingFor
     * @param $ageFrom
     * @param $ageTo
     * @param $distanceFrom
     * @param $distanceTo
     * @param $scoreFrom
     * @param $scoreTo
     * @param array $tagNameList
     * @param int $commonPointsMin
     * @return array|string|bool
     */
    static public function findUsers(User $UserLookingFor, $ageFrom, $ageTo, $distanceFrom, $distanceTo, $scoreFrom, $scoreTo, $tagNameList = [], $commonPointsMin = 1)
    {/* Could surely be optimized by using a graph database and by speeding up the instantiation of "User" entities */
        $inClause = "";
        $tagBinding = [];
        if (count($tagNameList) !== 0) {
            $index = 0;
            $TagList = array();
            $tagBinding = array();
            foreach ($tagNameList as $tagName) {
                try {
                    $Tag = new Tag(['name' => $tagName]);
                    array_push($TagList, $Tag);
                    $tagBinding = array_merge($tagBinding, ["Tag". $index => $Tag->id]);
                    $index++;
                } catch (Exception $e) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], " new Tag(['name' => $tagName]) =>". $e->getMessage());
                    return (false);
                }
            }
            $inClause = self::prepareTaggedInClause($tagBinding, LogicalOperator::AND);
        }

        $idUserLookingFor = $UserLookingFor->id;
        $whereClause = "WHERE id != $idUserLookingFor AND age >= :ageFrom AND age <= :ageTo". $inClause;

        if (($userList = User::listByExtendWhereClause(['ageFrom' => $ageFrom, 'ageTo' => $ageTo] + $tagBinding, $whereClause)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__],  "User::listByExtendWhereClause(['ageFrom' => $ageFrom, 'ageTo' => $ageTo] + ". json_encode($tagBinding) .", $whereClause)");
            return (false);
        }
        else if ($userList == ReturnCase::Empty) {
            return ([]);
        }

        $users = array();
        foreach ($userList as $User) {
            try {
                array_push($users, new User(['dbObject' => $User]));
            } catch (Exception $e) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], "new User(['id' => $User->id]) =>". $e->getMessage());
                return (false);
            }
        }

        /**
         * Filter by :
         *  - score
         *  - attract
         *  - common points
         */
        foreach ($users as $idArray => $User) {
            if ($User->checkCompletion()) {

                $users[$idArray]->commonPoints = self::countCommonPoint($UserLookingFor, $User);
                $users[$idArray]->distance = self::distanceBetween($UserLookingFor->geoCoordinates, $User->geoCoordinates);
                $users[$idArray]->compatibility = self::calcCompatibility($UserLookingFor, $User);

                /**
                 * $User->isBlockedBy($UserLookingFor->id) can generate Exception
                 */
                try {
                    if ($User->isBlockedBy($UserLookingFor->id) ||
                        $User->score < $scoreFrom || $User->score > $scoreTo ||
                        $User->distance < $distanceFrom || $User->distance > $distanceTo ||
                        self::checkAttraction($UserLookingFor, $User) === false ||
                        $users[$idArray]->commonPoints < $commonPointsMin)
                        unset($users[$idArray]);
                } catch (Exception $e) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 7, __FUNCTION__], " \$User->isBlockedBy($UserLookingFor->id) =>". $e->getMessage());
                    unset($users[$idArray]);
                }
            }
            else
                unset($users[$idArray]);
        }

        return ($users);
    }

    /* ====================== SORT SEARCH RESULT ====================== */

    /**
     * Sort the search results according to the type of sort and the order chosen
     *
     * @param array $resultList
     * @param string $sortReferenceAttribute (compatibility|score|distance|age|commonPoints)
     *     Attention: $sortReferenceAttribute must be correct, otherwise the method will return false and the sorting will not be done
     * @param string $order (ascending|descending)
     * @return array|bool
     */
    static public function sort(array $resultList , $sortReferenceAttribute = "compatibility", $order = "ascending")
    {
        if (empty($resultList)) {
            Session::flash('Warning', "No profiles matching the search were found");
            return ($resultList);
        }

        if (isset($resultList[key($resultList)]->$sortReferenceAttribute) == false) {
            Debug::print(SignalType::Error, 'SEARCH ENGINE SORT', [__FILE__, __LINE__ - 1, __FUNCTION__], "The attribut \"$sortReferenceAttribute\" does not exist in the class : \"". $resultList[key($resultList)]::getClassName() .'"');
            return (false);
        }

        $Sorter = new Sorter($sortReferenceAttribute, $order);
        usort($resultList, array($Sorter, 'integerAttributCompare'));

        return ($resultList);
    }

}
