<?php


namespace App\Libraries\Utils;


class Sorter
{
    public $sortReferenceAttribute;
    public $orderMultiplier = 1;

    public function __construct($sortReferenceAttribute, $order = "ascending")
    {
        $this->sortReferenceAttribute = $sortReferenceAttribute;
        if ($order == "descending")
            $this->orderMultiplier = -1;
    }

    public function integerAttributCompare($a, $b)
    {
        $attributName = $this->sortReferenceAttribute;

        if ($a->$attributName < $b->$attributName) {
            return (-1 * $this->orderMultiplier);
        }
        else if ($a->$attributName == $b->$attributName) {
            return (0);
        }
        else {
            return (1 * $this->orderMultiplier);
        }
    }
}
