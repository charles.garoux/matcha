<?php

namespace App\Libraries;

use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Logger;

class Debug
{
    /*
    ** Debug::print()
    **
    ** Cette methode a pour but d'afficher (ou logger) un message d'erreur
    **	$position format ["fichier", "ligne de l'origine", "function actuel"], la fonction actuel etant optionnel
    **   exemple :
    **		[__FILE__, __LINE__ - 1]
    **		[__FILE__, __LINE__ - 1, __FUNCTION__]
    **
    **	$critical_error = si vrai, force l'ecriture de l'erreur cote client
    **  $signalType = si different a SignalType::Error et DEBUG=LOG, le message ne sera pas enregistré dans les logs
    */

    static public function print($signalType, $component, $position, $message, $critical_error = false)
    {
        $signalName = strtoupper($signalType);

        if ($component == null)
            $componentName = '';
        else
            $componentName = " [$component]";

        if ($position!= null) {
            $functionName = (isset($position[2]) ? " $position[2]()" : '');
            $positionString = $position[0] .'('. $position[1] .')'. $functionName;
        }
        else
            $positionString = null;

        $debug_log = "[$signalName]$componentName $positionString: $message". PHP_EOL;

        if (env('DEBUG') == 'TRUE' || $critical_error) {
            print ($debug_log);
        }
        else if (env('DEBUG') == 'LOG' && $signalType == SignalType::Error) {
            if (Logger::write($debug_log) === false) {
                self::print(SignalType::Error,"LOGGING", [__FILE__, __LINE__ - 1, __FUNCTION__],__FILE__.' write_in_log() '. (__LINE__ - 1), true);
                exit();
            }
        }
    }

    /*
    ** Debug::view() :
    **	Retourne une vue signalant a l'utilisateur qu'une erreur c'est produite
    **
    **	Methode a utiliser seulement a l'interieur de controller
    **
    **  INFORMATION : si $signalType vaut SignalType::Warning, il ne sera pas affiché (sauf avec DEBUG=TRUE)
    **                  et ne sera jamais enregistré dans les logs
    */

    static public function view($signalType, $component, $position, $debugMessage, $messageToUser = null)
    {
        self::print($signalType, $component, $position, $debugMessage, null);

        if ($signalType === SignalType::Error)
            $textColor = 'text-danger';
        else if ($signalType === SignalType::Warning)
            $textColor = 'text-warning';

        if (env('DEBUG') == 'TRUE')
            $message = ($messageToUser === null ? "" : $messageToUser);
        else if (env('DEBUG') == 'LOG')
            $message = ($messageToUser === null ? "A problem has occurred, its information has been recorded" : $messageToUser);
        else
            $message = ($messageToUser === null ? "A problem has occurred" : $messageToUser);

        return view('Utils.debugView', compact('textColor', 'message'));
    }
}


