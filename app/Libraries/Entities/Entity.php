<?php


namespace App\Libraries\Entities;

use \App\Libraries\DB\Tables\DB_Table;
use App\Libraries\Debug;
use App\Libraries\Utils\Enums\LogicalOperator;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use Exception;

class Entity extends DB_Table
{
    public $id;

    /**
     * Specials attributes
     *      Use to designate an other table with different name
     *      Example : "idSender" to "idUser"
     */
    private $foreignKeyRedictions = array(
        'idSender'=> 'idUser',
        'idTarget'=> 'idUser',
    );

    /**
     * @var array
     * Contains all the information to be saved in the database (key)
     * and the location in the entity (value).
     *
     * Example, retrieving the "login" that is contained in the "login" attribute of the entity :
        ['login' => 'login']
     * Example, retrieving the "idUser" that is contained in the "id" attribute of the "User" entity :
        ['idUser' => 'User.id']
     *
     */
    protected $toSave = array(

    );

    /**
     * @var bool
     * Indicate whether to use foreign entities and instantiate sub-objects
     */
    protected $useForeignEntity;

    /** (temporary method)
     * Return doc of the object attributes
     *
     * @param string (terminal|browser) $displayType
     * @return string
     */
    public function doc($displayType = "browser")
    {
        if ("browser")
            $lineBreak = "<br>";
        else
            $lineBreak = "\n";

        $toSaveString = "";
        foreach ($this->toSave as $attribut => $path) {
            $toSaveString .= "- $attribut => $path $lineBreak";
        }

        $foreignKeyRedictionsString = "";
        foreach ($this->foreignKeyRedictions as $attribut => $redirection) {
            $foreignKeyRedictionsString .= "- $attribut => $redirection $lineBreak";
        }

        $doc = <<< DOC
The attributes and his path, to save, of the element in the database $lineBreak
attribut => path in the object :$lineBreak
$toSaveString
$lineBreak
Redirects are used to designate another table with a different name : $lineBreak
$foreignKeyRedictionsString

DOC;


        return ($doc);
    }

    /**
     * Entity constructor.
     * @param array $kwargs
     *      ['id'] => int : Initialize with DB data
     *      [...(see the format below)...] : Initialize with the first DB data found with input attributes (be precise)
     *             Format : ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @param bool $useForeignEntity
     * @throws Exception
     */
    public function __construct(array $kwargs, bool $useForeignEntity = true)
    {
        $this->useForeignEntity = $useForeignEntity;
        $this->find($kwargs);
    }

    /**
     * Get all attributes from DB table and tables pointed by foreign key
     * and store it in the instance then return the instance
     *
     * @param array $kwargs :
     *      If key "id" is set => use the id to find
     *      else => use all the attribut
     * @param string LogicalOperator::operator $logicalOperator :
     *      used for the multiple key search
     * @return Entity
     * @throws Exception
     */
    public function find(array $kwargs, $logicalOperator = LogicalOperator::AND)
    {
        if (isset($kwargs['id']))
            $EntityDatas = self::getBy('id', $kwargs['id']);
        else if (isset($kwargs['dbObject']))
            $EntityDatas = $kwargs['dbObject'];
        else
            $EntityDatas = self::getByExtend($kwargs, $logicalOperator);

        if ($EntityDatas === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' '. self::getClassName() .'::getBy...() === false');
            throw new Exception("Error from the DB", 100);
        }
        else if ($EntityDatas == ReturnCase::Empty)
            throw new Exception("Searched entity doesn't exist", 101);

        $this->toSave = $this->getDefaultAttributToSave($EntityDatas);

        foreach ($EntityDatas as $attribut => $value) {
            if ($this->useForeignEntity && self::isForeignKey($attribut)) {
                $ClassName = self::getForeignEntity($attribut);
                if (isset($this->foreignKeyRedictions[$attribut]))
                    $targetClass = "\App\Libraries\Entities\\". substr($this->foreignKeyRedictions[$attribut], 2);
                else
                    $targetClass = "\App\Libraries\Entities\\$ClassName";

                try {
                    $this->$ClassName = new $targetClass(['id' => $value]);
                } catch (\Exception $e) {
                    $errorCode = $e->getCode();
                    if ($errorCode == 100) {
                        Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], " $targetClass::getBy(\'id\', $value) " . $e->getMessage());
                        throw new Exception("Error from the DB", 100);
                    }
                    if ($errorCode == 101)
                        throw new Exception("Searched $targetClass entity doesn't exist", 101);
                }
            }
            else
                $this->$attribut = $value;
        }

        return ($this);
    }

    static private function isForeignKey($attributName) {
        if (strpos($attributName, 'id') === 0 && $attributName != 'id')
            return (true);
        return (false);
    }

    static private function getForeignEntity($attributName) {
        return (substr($attributName, 2));
    }

    private function getDefaultAttributToSave($DB_results) {
        $defaultToSave = array();
        foreach ($DB_results as $attributName => $value) {
            if ($this->useForeignEntity && self::isForeignKey($attributName)) {
                $target = self::getForeignEntity($attributName) . '.id';
            }
            else
                $target = $attributName;
            $defaultToSave = array_merge($defaultToSave, [$attributName => $target]);
        }
        return ($defaultToSave);
    }

    /**
     * Save (update) direct attributes (not those of other related objects) in DB
     *  Use by default $this->toSave
     *
     * @param array|null $toSave : Useful to indicate that certain attributes to update
     *      Format, see $this->toSave;
     *
     * @throws Exception
     */
    public function save(array $toSave = null) {

        if ($toSave == null)
            $toSave = $this->toSave;

        $arrayToSave = array();
        foreach ($toSave as $attributToSave => $targetData)
        {
            if (strpos($targetData, '.')) {
                $pathToTargetData = explode('.', $targetData);
                $data = $this;
                foreach ($pathToTargetData as $key => $value) {
                    $data = $data->$value;
                }
                $dataToSave = $data;
            }
            else
                $dataToSave = $this->$targetData;
            $arrayToSave = array_merge($arrayToSave, [$attributToSave => $dataToSave]);
        }

        if ($this::update($arrayToSave, ['id' => $this->id], LogicalOperator:: AND) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' '. self::getClassName() .'::update() === false');
            throw new Exception("Error from the DB, can't update", 102);
        }
    }

}
