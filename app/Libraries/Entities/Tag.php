<?php


namespace App\Libraries\Entities;


use App\Libraries\Debug;
use App\Libraries\Utils\Enums\SignalType;

class Tag extends Entity
{
    static public function listInJson()
    {
        if (($tagsList = Tag::listAll()) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "Tag::listAll() === false");
            $preparedTagsList = [];
        }
        else {
            $preparedTagsList = array();
            foreach ($tagsList as $key => $tag) {
                array_push($preparedTagsList, $tag->name);
            }
        }

        return (json_encode($preparedTagsList));
    }
}
