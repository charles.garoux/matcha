<?php


namespace App\Libraries\Entities;

use App\Libraries\Debug;
use App\Libraries\Utils\Enums\LogicalOperator;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\SearchEngine;
use App\Libraries\Utils\Session;
use \Exception;

class User extends Entity
{
    /**
     * @var array('tagName' => TagObject, 'tagName2' => TagObject) $tags
     *      TagObject est un objet de classe "Tag"
     */
    public $tags = [];

    /**
     * @var array('GenderName' => GenderObject, 'GenderName2' => GenderObject) $interests
     *      GenderObject est un objet de classe "Gender"
     */
    public $attractions = [];

    public $score;

    public $geoCoordinates;

    public $photos = ['profile' => null, 'other' => []];

    /**
     * User constructor.
     * @param array|null $kwargs
     * @param array|null $additionalAttributes : Optionals attributes
     *      Attributes :
     *             compatibility|distance|commonPoints (Calculate with respect to the logged in user)
     *      Example : ['distance' , 'compatibility']
     * @throws Exception
     */
    public function __construct(array $kwargs = null, array $additionalAttributes = null)
    {
        parent::__construct($kwargs);
        if ($kwargs == null)
            return;

        try {
            $this->getTags();
        } catch (Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ' $this->getTags() =>'. $e->getMessage());
            throw new Exception($e->getMessage(), $e->getCode());
        }

        try {
            $this->getAttractions();
        } catch (Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], '$this->getAttractions() =>'. $e->getMessage());
            throw new Exception($e->getMessage(), $e->getCode());
        }

        try {
            $this->score = $this->getScore();
        } catch (Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], '$this->getScore()'. $e->getMessage());
            throw new Exception($e->getMessage(), $e->getCode());
        }

        try {
            $this->geoCoordinates = $this->getGeoCoordinates();
        } catch (Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], '$this->getGeoCoordinates()'. $e->getMessage());
            throw new Exception($e->getMessage(), $e->getCode());
        }

        try {
            $this->photos = $this->getPhotos();
        } catch (Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], '$this->getGeoCoordinates()'. $e->getMessage());
            throw new Exception($e->getMessage(), $e->getCode());
        }

        /**
         * compatibility|distance|commonPoints
         */
        if ($additionalAttributes != null) {
            try {
                $idUser = Session::get('user.id');
                $User = new User(['id' => $idUser]);
            } catch (\Exception $e) {
                $errorCode = $e->getCode();
                if ($errorCode != 101)
                    return Debug::view(SignalType::Warning, 'DB', [__FILE__, __LINE__ - 4, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
                return Debug::view(SignalType::Error, 'DB', [__FILE__, __LINE__ - 5, __FUNCTION__], "new User(['id' => $idUser]) => ". $e->getMessage(), $e->getMessage());
            }
            if ($this->geoCoordinates !== 'Empty' && in_array('compatibility', $additionalAttributes))
                $this->compatibility = SearchEngine::calcCompatibility($User, $this);
            if ($this->geoCoordinates !== 'Empty' && in_array('distance', $additionalAttributes))
                $this->distance = SearchEngine::distanceBetween($User->geoCoordinates, $this->geoCoordinates);
            if (in_array('commonPoints', $additionalAttributes))
                $this->commonPoints = SearchEngine::countCommonPoint($User, $this);
        }
    }

    /**
     * If $this->tags is empty :
     *  Get the list of tags and store it in the instance
     * Return the list of tags
     *
     * @return array|string
     * @throws Exception
     */
    public function getTags() {
        if (empty($this->tags) == false)
            return ($this->tags);

        if (($tagsList = Tagged::listBy('idUser', $this->id)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' Tagged::listBy(\'idUser\', $this->id) === false');
            throw new Exception("Error from the DB", 100);
        }
        else if ($tagsList == ReturnCase::Empty)
            return (ReturnCase::Empty);
        $this->tags = array();
        foreach ($tagsList as $Tagged) {
            $Tag = Tag::getBy('id', $Tagged->idTag);
            $this->tags = array_merge($this->tags, array("$Tag->name" => $Tag));
        }

        return ($this->tags);
    }

    /**
     * If $this->attractions is empty :
     *  Get the list of attractions and store it in the instance
     * Return the list of attractions
     *
     * @return array|string
     * @throws Exception
     */
    public function getAttractions() {

        if (empty($this->attractions) == false)
            return ($this->attractions);

        if (($attractionsList = Attracted::listBy('idUser', $this->id)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' Tagged::listBy(\'idUser\', $this->id) === false');
            throw new Exception("Error from the DB", 100);
        }
        else if ($attractionsList == ReturnCase::Empty)
            return (ReturnCase::Empty);
        $this->attractions = array();
        foreach ($attractionsList as $Attracted) {
            $Gender = Gender::getBy('id', $Attracted->idGender);
            $this->attractions = array_merge($this->attractions, array("$Gender->name" => $Gender));
        }

        return ($this->attractions);
    }

    /**
     * If $this->geoCoordinates is empty :
     *  Get the user geoCoordinates and store it in the instance
     * Return the user geoCoordinates
     *
     * @return array|string
     * @throws Exception
     */
    public function getGeoCoordinates() {

        if (empty($this->geoCoordinates) == false)
            return ($this->geoCoordinates);

        if (empty($this->position))
            return (ReturnCase::Empty);

        if (is_array($this->geoCoordinates = unserialize($this->position)) === false) {
            Debug::print(SignalType::Error, 'ENTITY', [__FILE__, __LINE__ - 1, __FUNCTION__], " unserialize($this->position) === false");
            throw new Exception("Serialization error", 301);
        }

        return ($this->geoCoordinates);
    }

    private function getPhotos()
    {
        if (empty($this->photos['profile']) == false || empty($this->photos['other']) == false)
            return ($this->photos);

        if (($photosList = Photo::listBy('idUser', $this->id)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], ' Photo::listBy(\'idUser\', $this->id) === false');
            throw new Exception("Error from the DB", 100);
        }
        else if ($photosList == ReturnCase::Empty)
            return (ReturnCase::Empty);
        $this->photos = array();

        $this->photos['other'] = [];
        foreach ($photosList as $Photo) {
            try {
                $Photo = new Photo(['id' => $Photo->id], false);
                if ($Photo->profilePhoto == 1)
                    $this->photos['profile'] = $Photo;
                else
                    array_push($this->photos['other'], $Photo);
            } catch (Exception $e) {
                if ($e->getCode() === 100) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], '$this->getGeoCoordinates()'. $e->getMessage());
                    throw new Exception("Error from the DB", 100);
                }
            }
        }

        return ($this->photos);
    }

    /* =============== SCORE =============== */
    /**
     * Scoring scale :
     *  - a user visits the profile one or more times = +1 point
     *  - one match = +15 points
     *  - one like = +5 points
     *  - one message up to one hundred receipts per conversation = +[1,100] points
     *  - one report = -50 points
     */

    private function getScoreFromVisits($visits)
    {
        return (count($visits));
    }

    private function getScoreFromMatchs($matchs)
    {
        return (count($matchs) * 15);
    }

    private function getScoreFromLikes($likes)
    {
        return (count($likes) * 5);
    }

    private function getScoreFromConversationMessages($conversationMessages)
    {
        return ((count($conversationMessages) <= 100)? count($conversationMessages): 100);
    }

    private function getScoreFromReports($reports)
    {
        return (count($reports) * -50);
    }

    /**
     * Get user score
     *
     * @return int
     * @throws Exception
     */
    public function getScore() {
        if (empty($this->score) == false)
            return ($this->score);

        $this->score = 0;

        if (($visits = Visit::listDistinctVisits($this->id)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__],  " Visit::listBy('idTarget', $this->id) === false");
            throw new Exception("Error from the DB", 100);
        }
        else if (ReturnCase::isValidValue($visits) === false)
            $this->score += $this->getScoreFromVisits($visits);

        if (($matchs = Like::listMatchsByidUser($this->id)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Like::listMatchsByidUser($this->id) === false");
            throw new Exception("Error from the DB", 100);
        }
        else if (ReturnCase::isValidValue($matchs) === false)
            $this->score += $this->getScoreFromMatchs($matchs);

        if (($likes = Like::listBy('idTarget', $this->id)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Like::listBy('idTarget', $this->id) === false");
            throw new Exception("Error from the DB", 100);
        }
        else if (ReturnCase::isValidValue($likes) === false)
            $this->score += $this->getScoreFromLikes($likes);

        if ($matchs != ReturnCase::Empty) {
            foreach ($matchs as $match) {
                if (($conversationMessages = Message::listConversationMessages($this->id, $match->idTarget)) === false) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Message::listConversationMessages($this->id, $match->idTarget) === false");
                    throw new Exception("Error from the DB", 100);
                }
                else if (ReturnCase::isValidValue($conversationMessages) === false) {
                    $this->score += $this->getScoreFromConversationMessages($conversationMessages);
                }
            }
        }

        if (($reports = Report::listBy('idTarget', $this->id)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__],  " Report::listBy('idTarget', $this->id) === false");
            throw new Exception("Error from the DB", 100);
        }
        else if (ReturnCase::isValidValue($reports) === false)
            $this->score += $this->getScoreFromReports($reports);

        return ($this->score);
    }



    /* =============== ACCOUNT COMPLETION =============== */

    public function checkCompletion()
    {
        /**
         * Check attributes from DB
         */
        if (empty($this->biography) ||
            empty($this->position)||
            empty($this->age)||
            empty($this->Gender)) {
            return (false);
        }

        /**
         * Check attributes prepared from DB
         */
        if (empty($this->tags) ||
            empty($this->attractions)||
            empty($this->photos['profile'])) {
            return (false);
        }

        return (true);
    }

    /* =============== BLOCKING =============== */


    public function isBlockedBy(int $idUser)
    {
        if (($block = Blocking::getByExtend(['idTarget' => $this->id, 'idSender' => $idUser], LogicalOperator::AND)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Blocking::getByExtend(['idTarget' => $this->id, 'idSender' => $idUser], LogicalOperator::AND) === false");
            throw new Exception("Error from the DB", 100);
        }
        else if ($block === ReturnCase::Empty) {
            return (false);
        }

        return (true);
    }

    /* =============== PROFILE UPDATE =============== */

    /**
     * Update of what attracts the profile
     *
     * @param array $attractionList
     *      Format : ['GenderA', 'GenderB', ...]
     * @throws Exception
     */
    public function updateAttraction(array $attractionList)
    {
        try {
            $this->updateExternalDataList('Gender', 'Attracted', $attractionList, $this->attractions);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__],  " \$this->updateExternalDataList('Gender', 'Attracted', ". json_encode($attractionList) .")");
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Update tags of the profile
     *
     * @param array $tagList
     *      Format : ['GenderA', 'GenderB', ...]
     * @throws Exception
     */
    public function updateTags(array $tagList)
    {
        try {
            $this->updateExternalDataList('Tag', 'Tagged', $tagList, $this->tags);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__],  " \$this->updateExternalDataList('Gender', 'Attracted', ". json_encode($tagList) .")");
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * External data update linked to the profile
     *
     * @param string $tableName : table 'Tag' or 'Gender'
     * @param string $linkTableName : table 'Tagged' or 'Attracted'
     * @param array $dataList
     *      Format : ['dataName1', 'dataName2', 'dataName3']
     * @param array $startList
     * @throws Exception
     */
    private function updateExternalDataList(string $tableName, string $linkTableName, array $dataList, $startList)
    {
        $class = "\App\Libraries\Entities\\$tableName";
        $linkClass = "\App\Libraries\Entities\\$linkTableName";
        $attractionsToDelete = (empty($startList) ? [] : $startList);


        foreach ($dataList as $dataName) {
            unset($attractionsToDelete[$dataName]);
            if (($data = $class::getBy('name', $dataName)) === false) {
                Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $class::getBy('name', $dataName) === false");
                throw new Exception("Error from the DB", 100);
            }
            else if ($data == ReturnCase::Empty) {
                if (($newElementId = $class::insert(['name' => $dataName])) === false) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $class::insert(['name' => $dataName]) === false");
                    throw new Exception("Error from the DB", 100);
                }
            }
            else {
                if (($linkData = $linkClass::getByExtend(['idUser' => $this->id, 'id'.$tableName => $data->id], LogicalOperator::AND)) === false) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $linkClass::getByExtend(['idUser' => $this->id, 'id'.$tableName => $data->id], LogicalOperator::AND) === false");
                    throw new Exception("Error from the DB", 100);
                }
                else if ($linkData === ReturnCase::Empty) {
                    $idData = ($newElementId ?? $data->id);

                    if (($linkData = $linkClass::insert(['idUser' => $this->id, 'id'.$tableName => $idData])) === false) {
                        Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $linkClass::insert(['idUser' => $this->id, 'id'.$tableName => $idData]) === false");
                        throw new Exception("Error from the DB", 100);
                    }
                }
            }
        }

        if (empty($attractionsToDelete) === false) {
            foreach ($attractionsToDelete as $attractionName => $attractionData) {
                if (($linkData = $linkClass::deleteByExtend(['idUser' => $this->id, 'id'.$tableName => $attractionData->id], LogicalOperator::AND)) === false) {
                    Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " $linkClass::deleteByExtend(['idUser' => $this->id, 'id'.$tableName => $attractionData->id], LogicalOperator::AND) === false");
                    throw new Exception("Error from the DB", 100);
                }
            }
        }
    }

}
