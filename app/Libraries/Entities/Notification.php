<?php


namespace App\Libraries\Entities;


use App\Libraries\Debug;
use App\Libraries\Utils\Enums\LogicalOperator;
use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Utils\Enums\SignalType;
use App\Libraries\Utils\Session;
use Illuminate\Support\Facades\DB;

class Notification extends Entity
{
    /**
     * List all notifications not views
     *
     * @param $idUser
     * @return array|bool
     */
    static public function listNotViews($idUser)
    {
        $bindings = ['idUser' => $idUser, 'isSeen' => false];
        $request = <<< REQUEST
SELECT * FROM `Notification`
REQUEST;

        $whereClause = self::prepareWhereClause($bindings, LogicalOperator::AND);
        $request .= $whereClause .' ORDER BY timestamp DESC';

        try {
            $result = DB::select($request, $bindings);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }
        return (self::check($result));
    }

    /**
     * Check if the use can push a notification to the target user
     * then create the notification
     *
     * @param $idUser
     * @param $message
     * @param string $link : the link to redirect when the notification is select
     * @return int|string|bool :
     *          Integer is the notification id
     *          ReturnCase::Deny in case the user is not authorized to produce a notification
     *          False if an error with the DB
     */
    static public function push($idUser, $message, $link = "")
    {
        $idUserInitiating = Session::get('user.id');

        if (($block = Blocking::getByExtend(['idTarget' => $idUserInitiating, 'idSender' => $idUser], LogicalOperator::AND)) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " Blocking::getByExtend(['idTarget' => $idUserInitiating, 'idSender' => $idUser], LogicalOperator::AND) === false");
            return (false);
        }
        else if ($block !== ReturnCase::Empty) {
            return (ReturnCase::Deny);
        }

        if (($idNotification = self::insert(['idUser' => $idUser, 'message' => $message, 'link' => $link])) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], " self::insert(['idUser' => $idUser, 'message' => $message, 'link' => $link]) === false");
            return (false);
        }

        return ($idNotification);
    }
}
