<?php


namespace App\Libraries\Entities;

use App\Libraries\DB\Tables;

class Ticket extends Entity
{
    static public function generator($len = 40)
    {
        $charList = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charListLen = strlen($charList);
        $key = '';
        for ($i = 0; $i < $len; $i++) {
            $key .= $charList[rand(0, $charListLen - 1)];
        }
        return $key;
    }
}
