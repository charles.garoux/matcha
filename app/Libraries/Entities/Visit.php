<?php


namespace App\Libraries\Entities;


use App\Libraries\Debug;
use App\Libraries\Utils\Enums\SignalType;
use Illuminate\Support\Facades\DB;

class Visit extends Entity
{
    static public function listDistinctVisits($idTarget) {
        $request = <<< REQUEST
SELECT *, DATE_FORMAT(date, '%e-%m-%Y') AS formatedDate
FROM Visit
WHERE idTarget = :idTarget
GROUP BY formatedDate, idSender
ORDER BY date DESC
REQUEST;

        try {
            $result = DB::select($request, ['idTarget' => $idTarget]);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }

        return (self::check($result));
    }
}
