<?php


namespace App\Libraries\Entities;


use App\Libraries\Debug;
use App\Libraries\Utils\Enums\SignalType;
use Illuminate\Support\Facades\DB;

class Message extends Entity
{
    static public function listConversationMessages($idUser, $idContact)
    {
        $request = <<< REQUEST
SELECT * FROM `Message` WHERE
(idtarget = :idUser AND idSender = :idContact) OR
(idtarget = :idContactBis AND idSender = :idUserBis)
ORDER BY timestamp
REQUEST;

        try {
            $result = DB::select($request, ['idUser' => $idUser, 'idContact' => $idContact, 'idContactBis' => $idContact, 'idUserBis' => $idUser]); // Peu etre amelioré
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }

        return (self::check($result));
    }
}
