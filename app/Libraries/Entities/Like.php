<?php


namespace App\Libraries\Entities;


use App\Libraries\Debug;
use App\Libraries\Utils\Enums\SignalType;
use Illuminate\Support\Facades\DB;

class Like extends Entity
{
    static public function listMatchsByidUser($idUser)
    {
        $request = <<< REQUEST
SELECT * FROM
(SELECT *, id AS idLikeA FROM `Like` WHERE idTarget = :idTarget) AS LikeA
INNER JOIN
(SELECT *, id AS idLikeB FROM `Like` WHERE idSender = :idSender) AS LikeB
ON LikeA.idSender = LikeB.idTarget
REQUEST;

        try {
            $result = DB::select($request, ['idTarget' => $idUser, 'idSender' => $idUser]);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }

        return (self::check($result));
    }

    static public function getMatch($idUserA, $idUserB)
    {
        $request = <<< REQUEST
SELECT * FROM
(SELECT *, id AS idLikeA FROM `Like` WHERE idTarget = :idTargetA AND idSender = :idSenderA) AS LikeA
INNER JOIN
(SELECT *, id AS idLikeB FROM `Like` WHERE idSender = :idSenderB AND idTarget = :idTargetB) AS LikeB
ON LikeA.idSender = LikeB.idTarget;
REQUEST;

        try {
            $result = DB::select($request, ['idTargetA' => $idUserA, 'idSenderB' => $idUserA, 'idTargetB' => $idUserB, 'idSenderA' => $idUserB]);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }

        return (self::getFirst($result));
    }
}
