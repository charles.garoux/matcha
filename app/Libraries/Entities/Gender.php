<?php


namespace App\Libraries\Entities;


use App\Libraries\Debug;
use App\Libraries\Utils\Enums\SignalType;

class Gender extends Entity
{
    static public function listInJson()
    {
        if (($gendersList = Gender::listAll()) === false) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 1, __FUNCTION__], "Gender::listAll() === false");
            $preparedGendersList = [];
        }
        else {
            $preparedGendersList = array();
            foreach ($gendersList as $key => $gender) {
                array_push($preparedGendersList, $gender->name);
            }
        }

        return (json_encode($preparedGendersList));
    }
}
