<?php

namespace App\Libraries\DB\Tables;

use App\Libraries\Utils\Enums\SignalType;
use Illuminate\Support\Facades\DB;
Use App\Libraries\Utils\Enums\ReturnCase;
use App\Libraries\Debug;
Use App\Libraries\Utils\Enums\LogicalOperator;


/**
 * ATTENTION : Ne pas utiliser directement cette classe, il faut utiliser ses classes filles
 */
abstract class DB_Table
{
	/* ================ INSTRUCTION ET NORME DE NOMMAGE ================ */

    /**
     * Prefixes de methode :
     *    get    = retourne le 1er element trouve
     *    list = retourne la liste des elements
     *    delete = supprime tous les elements correspondant
     */

    /**
     * Sufixes de methode :
     *    Extend    = utilise un tableau avec comme cles les noms d'attribut et valeur les valeur d'attribut cherche
     *      Exemple : ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     */

    /* ================ METHODE UTILITAIRE ================ */

    /**
     * Get the class name
     *
     * @return string
     */
    static public function getClassName()
    {
        return (substr(strrchr(get_called_class(), "\\"), 1));
    }

    /**
     * Filter the result
     *
     * @param array $result
     * @return mixed $result
     */
    static protected function check($result)
    {
        if (count($result) === 0 || $result == NULL)
            return (ReturnCase::Empty);
        if (ReturnCase::isValidValue($result) || $result !== false)
            return ($result);
        return (false);
    }

    /**
     * Filter the result then return the first element
     *
     * @param array $result
     * @return array $result
     */
    static protected function getFirst($result)
    {
        $result = self::check($result);
        return (is_array($result) ? $result[0] : $result);
    }

    /**
     * Transform a bindings array into an SQL WHERE clause
     *
     * @param array $bindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @param LogicalOperator::operator $logicalOperator
     * @return string
     */
    static protected function prepareWhereClause($bindings, $logicalOperator)
    {
        $whereClause = null;
        foreach ($bindings as $attributName => $attributValue) {
            if ($whereClause === null)
                $whereClause = " WHERE `$attributName` = :$attributName";
            else
                $whereClause .= " $logicalOperator `$attributName` = :$attributName";
        }

        return ($whereClause);
    }

    /**
     * Transform a bindings array into SQL INSERT and VALUES clauses
     *
     * @param array $bindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @return array
     */
    static protected function prepareInsertClause($bindings)
    {
        $columnClause = null;
        $valuesClause = null;
        foreach ($bindings as $attributName => $attributValue) {
            if ($columnClause == null) {
                $columnClause = "(`$attributName`";
                $valuesClause = "VALUES (:$attributName";
            } else {
                $columnClause .= ", `$attributName`";
                if (gettype($attributValue) == 'integer')
                    $valuesClause .= ", :$attributName";
                else
                    $valuesClause .= ", :$attributName";
            }
        }
        $columnClause .= ')';
        $valuesClause .= ')';

        return (array('columnClause' => $columnClause, 'valuesClause' => $valuesClause));
    }

    /**
     * Transform a bindings array into an SQL SET clause
     *
     * @param array &$bindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @return string
     */
    static public function prepareSetClause(&$bindings)
    {
        $setClause = null;
        foreach ($bindings as $attributName => $attributValue) {

            if ($setClause === null)
                $setClause = " SET `$attributName` = :set_$attributName";
            else
                $setClause .= ", $attributName = :set_$attributName";
        }

        foreach ($bindings as $attributName => $attributValue) {
            $bindings['set_' . $attributName] = $bindings[$attributName];
            unset($bindings[$attributName]);
        }

        return ($setClause);
    }

    /* ================================ METHODE OUTILS ================================ */
    /* Ces methodes permet de faire des actions de cherche ou de suppression
    ** selon l'attribut et la valeur entrees. Pour les utiliser l'attribut doit exister
    ** dans la table correspondantes.
    */

    /**
     * Get element by attribut/value
     *
     * @param string $attribut
     * @param mixed $value
     * @return array|bool $result
     */
    static public function getBy($attribut, $value)
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
SELECT * FROM `$tableName` WHERE `$attribut` = :$attribut LIMIT 1
REQUEST;

        try {
            $result = DB::select($request, ["$attribut" => $value]);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }
        return (self::getFirst($result));
    }

    /**
     * List element by attribut/value
     *
     * @param string $attribut
     * @param mixed $value
     * @return array|bool $result
     */
    static public function listBy($attribut, $value)
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
SELECT * FROM `$tableName` WHERE `$attribut` = :$attribut
REQUEST;

        try {
            $result = DB::select($request, ["$attribut" => $value]);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }
        return (self::check($result));
    }

    /**
     * Delete element(s) by attribut/value
     *
     * @param $attribut
     * @param $value
     * @return int $nbrDeleted
     */
    static public function deleteBy($attribut, $value)
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
DELETE FROM `$tableName` WHERE `$attribut` = :$attribut
REQUEST;

        try {
            $nbrDeleted = DB::delete($request, ["$attribut" => $value]);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::delete() ' . $e->getMessage());
            return (false);
        }
        return ($nbrDeleted);
    }

    /**
     * Get element with many attribut/value
     *
     * @param array $bindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @param LogicalOperator::operator $logicalOperator
     * @return array|bool
     */
    static public function getByExtend($bindings, $logicalOperator)
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
SELECT * FROM `$tableName`
REQUEST;
        $endRequest = " LIMIT 1";

        $whereClause = self::prepareWhereClause($bindings, $logicalOperator);
        $request .= $whereClause . $endRequest;

        try {
            $result = DB::select($request, $bindings);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }
        return (self::getFirst($result));
    }

    /**
     * List elements with many attribut/value
     *
     * @param array $bindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @param LogicalOperator::operator $logicalOperator
     * @return array|bool
     */
    static public function listByExtend($bindings, $logicalOperator)
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
SELECT * FROM `$tableName`
REQUEST;

        $whereClause = self::prepareWhereClause($bindings, $logicalOperator);
        $request .= $whereClause;

        try {
            $result = DB::select($request, $bindings);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }
        return (self::check($result));
    }

    /**
     * Delete element(s) with many attribut/value
     *
     * @param array $bindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @param LogicalOperator::operator $logicalOperator
     * @return int|bool $nbrDeleted
     */
    static public function deleteByExtend($bindings, $logicalOperator)
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
DELETE FROM `$tableName`
REQUEST;

        $whereClause = self::prepareWhereClause($bindings, $logicalOperator);
        $request .= $whereClause;

        try {
            $nbrDeleted = DB::delete($request, $bindings);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::delete() ' . $e->getMessage());
            return (false);
        }
        return ($nbrDeleted);
    }

    /**
     * Inset a new element
     *
     * @param array $bindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @return int lastInsertId()
     */
    static public function insert($bindings)
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
INSERT INTO `$tableName`
REQUEST;

        $insertClauses = self::prepareInsertClause($bindings);
        $request .= ' ' . $insertClauses['columnClause'] . ' ' . $insertClauses['valuesClause'];

        try {
            DB::insert($request, $bindings);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::insert() ' . $e->getMessage());
            return (false);
        }
        return (DB::getPdo()->lastInsertId());
    }

    /**
     * Update datas
     *
     * @param array $updateBindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @param array $whereBindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @param LogicalOperator::operator $logicalOperator
     * @return int|bool $nbrUpdated
     */
    static public function update($updateBindings, $whereBindings, $logicalOperator)
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
UPDATE `$tableName`
REQUEST;

        $setClause = self::prepareSetClause($updateBindings);
        $whereClause = self::prepareWhereClause($whereBindings, $logicalOperator);
        $request .= $setClause . $whereClause;
        $bindings = array_merge($updateBindings, $whereBindings);

        try {
            $nbrUpdated = DB::update($request, $bindings);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::update() ' . $e->getMessage());
            return (false);
        }
        return ($nbrUpdated);
    }

    /**
     * Get element with many attribut/value and condition
     *
     * @param array $bindings ['attributNameA' => $attributValueA, 'attributNameB' => $attributValueB, ...]
     * @param string $whereClause : like "WHERE ..... OR .... AND ....."
     *      Replace attribut value by ":attributNameA", ":attributNameB", ...
     * @return array|bool
     */
    static public function listByExtendWhereClause($bindings, $whereClause = "")
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
SELECT * FROM `$tableName`
REQUEST;

        $request .= $whereClause;

        try {
            $result = DB::select($request, $bindings);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }
        return (self::check($result));
    }

    /* ================================ METHODE COMMUNE ================================ */
    /* Ces methodes fonctionnent pour toutes les tables */

    /**
     * List all elements
     *
     * @return array|bool $result
     */
    static public function listAll()
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
			SELECT * FROM `$tableName`
REQUEST;

        try {
            $result = DB::select($request);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }
        return (self::check($result));
    }

    /**
     * List all last record elements
     *
     * @param int $nbrLastRecord
     * @return array|bool $result
     */
    static public function listLastRecord($nbrLastRecord)
    {
        $tableName = self::getClassName();
        $request = <<< REQUEST
SELECT * FROM `$tableName` ORDER BY ID DESC LIMIT $nbrLastRecord
REQUEST;

        try {
            $result = DB::select($request);
        } catch (\Exception $e) {
            Debug::print(SignalType::Error, 'DB', [__FILE__, __LINE__ - 2, __FUNCTION__], ': DB::select() ' . $e->getMessage());
            return (false);
        }
        return (self::check($result));
    }

}
